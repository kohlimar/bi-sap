= 4. Proseminář: Struktura procesoru AVR. Asembler.
:imagesdir: ../../media/seminars/04


* link:{imagesdir}/sap-avr-prehled.pdf[Procesor AVR - přehled]
* link:{imagesdir}/sap-avr-priklady.pdf[Assembler AVR - příklady]

== Video

* https://youtu.be/qoGCU7fH9GE[🎦 Záznam 2021.1]
* https://youtu.be/Aax-nwARtns[🎦 Záznam 2021.2]
* https://avc.fit.cvut.cz/videa/FIT/SAP/2012/mp4/SAP_proseminar_4_4.4.2013_AVR.mp4[🎦 Záznam 2013]

== Odkazy
V xref:../../labs/common-avr/index#[odkazech] mj. najdete

* xref:../../labs/common-avr/instruction-list#[přehled instrukcí] (česky), 
* xref:../../labs/common-avr/instruction-encoding#[kódování instrukcí] (česky), 
* kompletní link:{imagesdir}/../../labs/common-avr/avrinstructionset.pdf[instrukční sadu] a 
* datasheet (katalogový list) link:{imagesdir}/../../labs/common-avr/atmega169vl.pdf[procesoru ATmega169]


'''

* link:{imagesdir}/vestavne-systemy.pdf[Vestavné systémy]
* https://fit.cvut.cz/cs/veda-a-vyzkum/zazemi/laboratore/12218-hardwarova-dilna-a-laborator-hwlab[Hardwarová dílna], https://hwlab.fit.cvut.cz/[interní stránky]
//* link:{imagesdir}/PocitacoveInzenyrstvi-poster2.2.pdf[Obor Počítačové inženýrství]

.Obor Počítačové inženýrství
[#img-ComputeEngineering]
[link=https://courses.fit.cvut.cz/BI-SAP/media/seminars/04/PocitacoveInzenyrstvi.png]
image::PocitacoveInzenyrstvi.png[Počítačové inženýrství, 300]