= 8. Přednáška: Strojový kód, asembler, architektura souboru instrukcí 
:imagesdir: ../../media/lectures/08

== Podklady pro přednášku

link:{imagesdir}/sap-8-isa.pdf[sap-8-isa.pdf]

== Video
//- https://teams.microsoft.com/l/meetup-join/19%3ameeting_MmRmMWM1NmMtMzVhNi00ZGI4LTk1MjYtMDMyNGZjZWE1YmRi%40thread.v2/0?context=%7b%22Tid%22%3a%22f345c406-5268-43b0-b19f-5862fa6833f8%22%2c%22Oid%22%3a%222f077ae9-1a94-47d5-87d6-37e7030bfde1%22%2c%22IsBroadcastMeeting%22%3atrue%7d&btype=a&role=a[🎦 nahrávka]    

- [YT zde]



== Otázky z přednášky, které se mohou objevit v testech:

* Nakreslete vývojový diagram pro Instrukční cyklus a popište
* 