:imagesdir: ../../../media/labs/10
:commonavrdir: ../../common-avr

:toc:
:toclevels: 2

= Odpočítávací zařízení


Naprogramujte odpočítávací zařízení pro [line-through]#bombu# mikrovlnku, které se po spuštění/resetu zadá požadovaná hodnota a poté odstartuje odpočítávání, které se ukončí na hodnotě 0. Nyní ovšem bude použita *desítková soustava* a časové jednotky v řádu minuty:sekundy. Tj. buď budete muset naprogramovat rutiny na převod do desítkové soustavy, anebo můžete odpočítávat přímo decimálně – obě řešení jsou možná.

== Požadavky na úlohu


* na displeji se budou zobrazovat minuty a sekundy _(obě hodnoty se budou zobrazovat na 2 cifry v desítkové soustavě)_,
** volitelně můžete zobrazovat i desetiny/setiny sekund,
* hodnoty se budou postupně volit joystickem,
** nejdříve minuty poté, po potvrzení joystickem, sekundy,
** volba nahoru/dolu, stisk=potvrzení,
* po nastavení hodnot se stiskem spustí odpočet,
* po skončení odpočtu několikrát zablikají nuly a poté se zobrazí "běžící text" s informací o ukončení odpočtu _(např.: [line-through]#TO NAM TO ALE KRASNE BOUCHLO# VAJICKA UVARENA)_,
* při dalším stisku joysticku je možné provést nové odpočítávání.

Mikroprocesor je taktován kmitočtem 2 MHz. Ovšem není možné přesně měřit čas pomocí počítání taktů CPU (pomocí čekacích smyček). Pro získání plného počtu bodů je vyžadováno použití časovače a <<{commonavrdir}/interrupt#,přerušení>>.

== Bonusy navíc

Za jakoukoliv kreativní implementaci.
