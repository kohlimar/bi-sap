:imagesdir: ../../../media/labs/10
:commonavrdir: ../../common-avr

:toc:
:toclevels: 2

= Variace na hru PONG


Na přípravku AVR Butterfly vytvořte variaci na http://codeincomplete.com/games/pong/[hru PONG]. Při spuštění přípravku se na displeji objeví "menu" s položkou "START" a při stisknutí joysticku se spustí hra.

Ve hře se posunuje neustále obrazec _(hvězdička nebo jiný znak dle libosti řešitele)_ zleva doprava a poté obráceně v intervalu zvoleném řešitelem. Pokud je obrazec vlevo, musí hráč pohnout joystickem doprava a pokud je obrazec vpravo, musí pohnout joystickem doleva. Pokud hráč pohne špatně joystickem _(obrazec se nenachází na okraji displeje nebo hráč nestihl pohnout joystickem včas)_,tak hra končí a na display se vypíše nápis "LOSER" _(ten 3x zabliká a pak zůstane zobrazen)_. Při stisku joysticku se dále hráči zobrazí skóre hry _(kolikrát obrazec doputoval tam a zpět)_ a po dalším stisku se zobrazí "menu" a následně je možné spustit novou hru.

Cílem hry je co nejdéle posouvat obrazcem tam a zpět.

Pro získání plného počtu bodů je vyžadováno použití časovače a <<{commonavrdir}/interrupt#,přerušení>>.

== Požadavky na úlohu

* menu s příkazem "START",
* implementace hry _(postačí konstantní rychlost pohybu obrazce)_,
* zobrazení nápisu na konci hry a dosaženého skóre.

Poznámky:

* Pokud je položek v "menu" více, volí se pohybem joysticku nahoru/dolu a stiskem joysticku.
* Do "menu" je možné vstoupit kdykoliv stiskem joysticku a během hry se neposuzuje pohyb joysticku nahoru/dolu, pouze doleva/doprava/stisk.


== Bonusy navíc

Cvičící může udělit bonusové body za mimořádnou implementaci například za:

*  položky v menu:
**  nejvyšší dosažené skóre,
**  volitelná rychlost pohybu obrazce,
**  možnost přerušit a následně pokračovat ve hře,

*  průběžné zrychlování hry na aktuálně dosaženém skóre,
*  zajímavý nápad a implementace řešitele.
