library STD;
use STD.textio.all;                     -- basic I/O

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;
use IEEE.std_logic_textio.all;

entity running_light_test is
    generic(
        C_STEPS_COUNT   : integer := 32;
        C_COUNTER_DELAY : integer := 4        -- pulse delay
    );
    port(
        Clock   : out std_logic;
        Reset   : out std_logic;

        Pulse   : out std_logic;

        LD0     : in  std_logic;
        LD1     : in  std_logic;
        LD2     : in  std_logic;
        LD3     : in  std_logic;
        LD4     : in  std_logic;
        LD5     : in  std_logic;
        LD6     : in  std_logic;
        LD7     : in  std_logic;
        LD8     : in  std_logic;
        LD9     : in  std_logic;
        LD10    : in  std_logic;
        LD11    : in  std_logic;
        LD12    : in  std_logic;
        LD13    : in  std_logic;
        LD14    : in  std_logic;
        LD15    : in  std_logic
    );  
end running_light_test;

architecture behavior of running_light_test is

    signal rst  : std_logic := '0';
    signal clk  : std_logic := '1';

    signal Counter_value     : integer := 0;

    constant CLK_PERIOD      : time := 10 ns;
    
    signal count_en          : std_logic := '0';
    signal en_out            : std_logic := '0';
    signal led               : std_logic_vector(15 downto 0);

    signal terminate         : boolean;
                
    ATTRIBUTE X_INTERFACE_INFO : STRING;
    ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
    ATTRIBUTE X_INTERFACE_PARAMETER OF Reset: SIGNAL IS "XIL_INTERFACENAME Reset, POLARITY ACTIVE_HIGH";
    ATTRIBUTE X_INTERFACE_INFO OF Reset: SIGNAL IS "xilinx.com:signal:reset:1.0 Reset RST";
    ATTRIBUTE X_INTERFACE_PARAMETER OF Clock: SIGNAL IS "XIL_INTERFACENAME Clock, ASSOCIATED_RESET Reset, FREQ_HZ 100000000, PHASE 0.000";
    ATTRIBUTE X_INTERFACE_INFO OF Clock: SIGNAL IS "xilinx.com:signal:clock:1.0 Clock CLK";
    
    function replace(val : std_logic_vector) return STRING is
      variable Result : STRING(val'range);
    begin
      for i in val'range loop
        if (val(i) = '1') then
            Result(i)  := '*';
        else
            Result(i)  := ' ';
        end if;
      end loop;
      return Result;
    end function;
begin

    buzzer: process
        variable file_line    : line;
    begin
        wait for 1000 ns;
        if (not terminate) then
            write(file_line, string'("-->>> Simulation paused - press Run-all to continue! <<<--"));
            writeline(output, file_line);
        end if;
        wait;
    end process;
        
    process
    begin
        clk <= not clk;
        wait for CLK_PERIOD/2;
        if (terminate) then
            wait;
        end if;
    end process;

    Clock   <= clk;
    
    -- simulovany rychlejsi counter
    process (clk, rst)
    begin
        if rst = '1' then
            Counter_value <= 0;
        elsif (Clk'event and Clk = '1') then
            if count_en = '1' then
                Counter_value <= Counter_value + 1;
                if Counter_value = C_COUNTER_DELAY then
                    Counter_value <= 0;
                end if;
            end if;
        end if;
    end process;

    en_out  <= '1' when Counter_value = C_COUNTER_DELAY else '0';
    Pulse   <= en_out;

    led <= LD15 & LD14 & LD13 & LD12 & LD11 & LD10 & LD9 & LD8 & LD7 & LD6 & LD5 & LD4 & LD3 & LD2 & LD1 & LD0;

    stim_proc: process
        variable file_line        : line;
        variable stable           : boolean;
        variable var_no_of_errors : integer;
    begin
        terminate <= false;
        var_no_of_errors := 0;
        
        assert C_COUNTER_DELAY > 3 report "C_COUNTER_DELAY should be greater than 3." severity error;
        assert C_STEPS_COUNT > 15 report "C_STEPS_COUNT should be greater than 15." severity error;

        -- zacatek simulace
        write(file_line, string'("### Simulation start ###"));
        writeline(output, file_line); -- write to display    

        count_en <= '0';
        rst <= '1';
        wait until rising_edge(clk);
        wait for 1 ns;
        rst <= '0';
        
        wait for 4 ns;

        wait until rising_edge(clk);
        count_en <= '1';
        wait for (C_COUNTER_DELAY/2)*CLK_PERIOD;
        write(file_line, string'("Step:   0 |"));
        write(file_line, replace(led));
        write(file_line, string'("|"));
        writeline(output, file_line);

        for I in 1 to (C_STEPS_COUNT-1) loop
            wait until rising_edge(en_out);
            wait for (C_COUNTER_DELAY/2)*CLK_PERIOD;
            write(file_line, string'("Step: "));
            write(file_line, I, right, 3);
            write(file_line, string'(" |"));
            write(file_line, replace(led));
            write(file_line, string'("|"));
            writeline(output, file_line);
        end loop;
        count_en <= '0';
        -- konec simulace
        write(file_line, string'("### Simulation finished ###"));
        writeline(output, file_line);

        terminate <= true;

        wait;
    end process;

    Reset   <= rst;


end;
