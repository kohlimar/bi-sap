:imagesdir: ../../../media/labs/06  

:mywidth: 500
:mywidth2x: 1000

= Trezor

[NOTE]
V rámci domácí přípravy si přinejmenším nakreslete graf přechodů a výstupů pro požadovaný automat (a zvolené heslo). Detaily k novinkám jako jsou hranový detektor nebo připojení hodin na oscilátor budou vysvětleny na cvičení. 

Navrhněte trezor ovládaný tlačítkovým křížem (Up, Right, Down, Left). Trezor se otevře, když budou tlačítka zmáčknuta ve správném (Vámi zvoleném) pořadí. Heslo bude mít délku 3 stisknutí, tedy např. UDU (nahoru, dolů, nahoru). Pokud dojde při zadávání hesla k chybě, automat se vrátí do počátečního stavu (trezor tedy odemkne jakkoli dlouhá sekvence se správnými 3 tlačítky na konci). Po otevření trezoru ho půjde zamknout (navrátit se do počátečního stavu) pouze stisknutím prostředního tlačítka, kam bude umístěn RESET klopných obvodů.

Správné zadání kódu bude indikováno LED diodou. Pro účely testování můžete aktuální stav zobrazovat na displeji či LED diodách.

Hodiny klopných obvodů budou připojeny na 100 MHz oscilátor přípravku (_W5_). Musíte tedy řešit problém se zákmity tlačítek a přecházení automatu do dalšího stavu pouze jednou na stisknutí tlačítka. S řešením zákmitů vám pomůže dobře známý Debounce. Druhý problém má více možných řešení:

* Váš automat musí čekat na uvolnění tlačítek (a tím pádem mít podstatně více stavů)
* Využijete z předchozích cvičení (u čítače pro povolení čítání) známého vstupu klopných obvodů ClockEnable a (níže popsaný) hranový detektor

Následují obrázky zapojení bez a s využitím dodaných či vysvětlených součástek.

image::jerabst1_trezorZapojeni0.png[Zapojení,link={imagesdir}/jerabst1_trezorZapojeni0.png,width={mywidth}]
image::jerabst1_trezorZapojeni1.png[Zapojení se vstupním kodérem a hranovým detektorem – Ukázka simulace,link={imagesdir}/jerabst1_trezorZapojeni1.png,width={mywidth2x}]

== Vstupní kodér

Ke snížení počtu vstupů automatu můžete (ale nemusíte) použít dodaný link:{imagesdir}/jerabst1_trezorKoder.vhd[vstupní kodér ve VHDL], který tlačítka kóduje na dvoubitové binární číslo BUTTONS(BUTTONS1, BUTTONS0) takto:

* 00 - Up
* 01 - Right
* 10 - Down
* 11 - Left

Všimněte si, že kodér neodlišuje situaci, kdy není stiknuté žádné tlačítko a ani neřeší současný stisk více tlačítek. Jeho použití proto vyžaduje také níže popsaný hranový detektor a využití ClockEanble vstupů klopných obvodů.

[NOTE]
Ve skutečnosti kodér vrací poslední (v pořadí up, right, down, left) stisknuté tlačítko, v případě uvolněných tlačítek pak "00". 

== Hranový detektor

Hranový detektor se skládá z jednoho klopného obvodu, hradla AND a invertoru a jeho zapojení najdete na obrázku níže. Hranový detektor libovolně dlouhý puls v logické jedničce mění na jedničku po dobu pouze jednoho taktu. Pomocí něho tedy můžete na ClockEnable klopných obvodů v automatu přivést jedničku na dobu jednoho taktu kdykoli je stisknuto některé z tlačítek.

image::jerabst1_hranovyDetektorSchema.png[Hranový detektor – Schéma,link={imagesdir}/jerabst1_hranovyDetektorSchema.png,width={mywidth}]
image::jerabst1_hranovyDetektorSim.png[Hranový detektor – Ukázka simulace,link={imagesdir}/jerabst1_hranovyDetektorSim.png,width={mywidth2x}]

Jak je vidět ze simulace, hranový detektor z libovolně (alespoň 1 periodu hodin) dlouhého pulsu na vstupu (input) udělá na výstupu (output) puls do první náběžné hrany hodin (clk), což je přesně to, co potřebujeme.

== Bonus (1 bod)

Odemkněte trezor pouze po stisknutí správné kombinace ihned po resetu obvodu a ne na konci libovolně dlouhé sekvence jako ve výchozím zadání. Možností, jak toto provést, je více. Než se do řešení pustíte, zkuste se poradit s cvičícím o vhodnosti Vašeho nápadu.

== Testování simulací

Pro testování použijte soubor link:./trezor/l6_trezor_test.vhd[l6_trezor_test.vhd]. V souboru musíte a můžete provést některé úpravy:

* Na řádku 10 MUSÍTE zadat Váš kód pro odemčení.
* Na řádcích 14 a 44 MUSÍTE změnit název komponenty __trezor_vhdl__ na Vámi používaný název (typicky __design_1_vhdl__ při použití skriptu).
* Na řádcích 24, 32, 53 a řádcích následujících po nich můžete zakomentovat testovací výstupy aktuálního stavu nebo naopak odkomentováním přidat další signály, pokud máte více než 4 stavy automatu a 2 bity Vám tedy nestačí.

Na začátku testu se vypíše poznámka _Zacatek testu_, na konci testu (v čase 218 640 ns) pak „chybová“ hláška _Konec testu_. Nezapomeňte po odsimulování prvních 1 000 ns kliknout na Run-all, abyste simulaci pustili až do konce. Mezi oběma hláškami pak najdete výpis případných chyb.

Nezapomeňte, že musíte přepnout debounce do režimu simulace.
 