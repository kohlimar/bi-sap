library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity trezor_test is
end trezor_test;

architecture Behavioral of trezor_test is

--ZDE NAPISTE VAS 3MISTNY KOD K ODEMCENI
constant code : string := "XXX";
--constant code : string := "URDL";

-- ZDE UPRAVTE NAZEV VYGENEROVANE ENTITY
component trezor_vhdl is
   port (
      up     :  in  std_logic;
      right  :  in  std_logic;
      down   :  in  std_logic;
      left   :  in  std_logic;
      reset  :  in  std_logic;
      clk    :  in  std_logic;
      opened :  out std_logic;
-- STAVY AUTOMATU NA VYSTUP K OD/ZAKOMENTOVANI ZDE
--      state3 :  out std_logic;
--      state2 :  out std_logic;
      state1 :  out std_logic;
      state0 :  out std_logic
   );
end component;

-- STAVY AUTOMATU NA VYSTUP K OD/ZAKOMENTOVANI ZDE
--signal state3 : std_logic;
--signal state2 : std_logic;
signal state1 : std_logic;
signal state0 : std_logic;
signal up, right, down, left, reset, clk, opened : std_logic;
signal codeVector : std_logic_vector(1 to 6);
signal v1, v2 : std_logic_vector(1 to 8);
signal v3, v4, v5, v6, v7, v8 : std_logic_vector(1 to 6);
constant CLKP : time := 20 ns;

begin
-- ZDE UPRAVTE NAZEV VYGENEROVANE ENTITY
uut:trezor_vhdl port map (
    up => up,
    right => right,
    down => down,
    left => left,
    reset => reset,
    clk => clk,
    opened => opened,
-- STAVY AUTOMATU NA VYSTUP K OD/ZAKOMENTOVANI ZDE
--    state3 => state3,
--    state2 => state2,
    state1 => state1,
    state0 => state0
);

vectorPrc : process
begin
    codeVector <= "000000";
    for i in 1 to 3 loop
        if code(i)='R' then codeVector(2*i-1 to 2*i) <= "01"; end if;
        if code(i)='D' then codeVector(2*i-1 to 2*i) <= "10"; end if;
        if code(i)='L' then codeVector(2*i-1 to 2*i) <= "11"; end if;
    end loop;
    wait for 1 ns;
    
    v1 <= codeVector & codeVector(5 to 6);
    v2 <= codeVector(1 to 2) & codeVector;
    
    case code(1) is
        when 'U' =>
            v3 <= "010101";
            v4 <= "101010";
            v5 <= "111111";
            v6 <= "011011";
            v7 <= "101100";
            v8 <= "110001";
        when 'R' =>
            v3 <= "000000";
            v4 <= "101010";
            v5 <= "111111";
            v6 <= "000110";
            v7 <= "101100";
            v8 <= "110001";
        when 'D' =>
            v3 <= "000000";
            v4 <= "010101";
            v5 <= "111111";
            v6 <= "000110";
            v7 <= "011011";
            v8 <= "110001";
        when others =>
            v3 <= "000000";
            v4 <= "010101";
            v5 <= "101010";
            v6 <= "000110";
            v7 <= "011011";
            v8 <= "101100";
    end case;            
    wait;
end process;

clkPrc : process
begin
    clk <= '0';
    wait for CLKP/2;
    clk <= '1';
    wait for CLKP/2;
end process;

main : process
begin
assert false report "Zacatek testu" severity note;

reset <= '1';
up <= '0'; right <= '0'; down <= '0'; left <= '0';
wait for 500*CLKP;
reset <= '0';

--v1
for i in 1 to 4 loop
    if v1(2*i-1 to 2*i) = "00" then up <= '1'; end if;
    if v1(2*i-1 to 2*i) = "01" then right <= '1'; end if;
    if v1(2*i-1 to 2*i) = "10" then down <= '1'; end if;
    if v1(2*i-1 to 2*i) = "11" then left <= '1'; end if;
    wait for 300*CLKP;
    up <= '0'; right <= '0'; down <= '0'; left <= '0';
    wait for 100*CLKP;
    if i>2 then
        assert opened='1' report "Chyba v prvnim vektoru - spravny kod s poslednim znakem jeste jednou." severity error;
    else
        assert opened='0' report "Chyba v prvnim vektoru - spravny kod s poslednim znakem jeste jednou." severity error;
    end if;
end loop;
reset <= '1'; wait for 3*CLKP; reset <='0'; wait for CLKP;

--v2
for i in 1 to 4 loop
    if v2(2*i-1 to 2*i) = "00" then up <= '1'; end if;
    if v2(2*i-1 to 2*i) = "01" then right <= '1'; end if;
    if v2(2*i-1 to 2*i) = "10" then down <= '1'; end if;
    if v2(2*i-1 to 2*i) = "11" then left <= '1'; end if;
    wait for 300*CLKP;
    up <= '0'; right <= '0'; down <= '0'; left <= '0';
    wait for 100*CLKP;
    if i=3 and code(1)=code(2) and code(1)=code(3) then
        assert opened='1' report "Chyba v druhem vektoru - spravny kod po prvnim znaku dvakrat." severity error;
    elsif i<4 then
        assert opened='0' report "Chyba v druhem vektoru - spravny kod po prvnim znaku dvakrat." severity error;
    else
        assert opened='1' report "Chyba v druhem vektoru - spravny kod po prvnim znaku dvakrat." severity error;
    end if;
end loop;
reset <= '1'; wait for 3*CLKP; reset <='0'; wait for CLKP;

--v3
for i in 1 to 3 loop
    if v3(2*i-1 to 2*i) = "00" then up <= '1'; end if;
    if v3(2*i-1 to 2*i) = "01" then right <= '1'; end if;
    if v3(2*i-1 to 2*i) = "10" then down <= '1'; end if;
    if v3(2*i-1 to 2*i) = "11" then left <= '1'; end if;
    wait for 300*CLKP;
    up <= '0'; right <= '0'; down <= '0'; left <= '0';
    wait for 100*CLKP;
    case v3(1 to 2) is
        when "00" => assert opened='0' report "Chyba ve tretim vektoru - UUU" severity error;
        when "01" => assert opened='0' report "Chyba ve tretim vektoru - RRR" severity error;
        when "10" => assert opened='0' report "Chyba ve tretim vektoru - DDD" severity error;
        when others => assert opened='0' report "Chyba ve tretim vektoru - LLL" severity error;
    end case;
end loop;
reset <= '1'; wait for 3*CLKP; reset <='0'; wait for CLKP;

--v4
for i in 1 to 3 loop
    if v4(2*i-1 to 2*i) = "00" then up <= '1'; end if;
    if v4(2*i-1 to 2*i) = "01" then right <= '1'; end if;
    if v4(2*i-1 to 2*i) = "10" then down <= '1'; end if;
    if v4(2*i-1 to 2*i) = "11" then left <= '1'; end if;
    wait for 300*CLKP;
    up <= '0'; right <= '0'; down <= '0'; left <= '0';
    wait for 100*CLKP;
    case v4(1 to 2) is
        when "00" => assert opened='0' report "Chyba ve ctvrtem vektoru - UUU" severity error;
        when "01" => assert opened='0' report "Chyba ve ctvrtem vektoru - RRR" severity error;
        when "10" => assert opened='0' report "Chyba ve ctvrtem vektoru - DDD" severity error;
        when others => assert opened='0' report "Chyba ve ctvrtem vektoru - LLL" severity error;
    end case;
end loop;
reset <= '1'; wait for 3*CLKP; reset <='0'; wait for CLKP;

--v5
for i in 1 to 3 loop
    if v5(2*i-1 to 2*i) = "00" then up <= '1'; end if;
    if v5(2*i-1 to 2*i) = "01" then right <= '1'; end if;
    if v5(2*i-1 to 2*i) = "10" then down <= '1'; end if;
    if v5(2*i-1 to 2*i) = "11" then left <= '1'; end if;
    wait for 300*CLKP;
    up <= '0'; right <= '0'; down <= '0'; left <= '0';
    wait for 100*CLKP;
    case v5(1 to 2) is
        when "00" => assert opened='0' report "Chyba v patem vektoru - UUU" severity error;
        when "01" => assert opened='0' report "Chyba v patem vektoru - RRR" severity error;
        when "10" => assert opened='0' report "Chyba v patem vektoru - DDD" severity error;
        when others => assert opened='0' report "Chyba v patem vektoru - LLL" severity error;
    end case;
end loop;
reset <= '1'; wait for 3*CLKP; reset <='0'; wait for CLKP;

--v6
for i in 1 to 3 loop
    if v6(2*i-1 to 2*i) = "00" then up <= '1'; end if;
    if v6(2*i-1 to 2*i) = "01" then right <= '1'; end if;
    if v6(2*i-1 to 2*i) = "10" then down <= '1'; end if;
    if v6(2*i-1 to 2*i) = "11" then left <= '1'; end if;
    wait for 300*CLKP;
    up <= '0'; right <= '0'; down <= '0'; left <= '0';
    wait for 100*CLKP;
    case v6(1 to 2) is
        when "00" => assert opened='0' report "Chyba v sestem vektoru - URD" severity error;
        when "01" => assert opened='0' report "Chyba v sestem vektoru - RDL" severity error;
        when "10" => assert opened='0' report "Chyba v sestem vektoru - DLU" severity error;
        when others => assert opened='0' report "Chyba v sestem vektoru - LUR" severity error;
    end case;
end loop;
reset <= '1'; wait for 3*CLKP; reset <='0'; wait for CLKP;

--v7
for i in 1 to 3 loop
    if v7(2*i-1 to 2*i) = "00" then up <= '1'; end if;
    if v7(2*i-1 to 2*i) = "01" then right <= '1'; end if;
    if v7(2*i-1 to 2*i) = "10" then down <= '1'; end if;
    if v7(2*i-1 to 2*i) = "11" then left <= '1'; end if;
    wait for 300*CLKP;
    up <= '0'; right <= '0'; down <= '0'; left <= '0';
    wait for 100*CLKP;
    case v6(1 to 2) is
        when "00" => assert opened='0' report "Chyba v sedmem vektoru - URD" severity error;
        when "01" => assert opened='0' report "Chyba v sedmem vektoru - RDL" severity error;
        when "10" => assert opened='0' report "Chyba v sedmem vektoru - DLU" severity error;
        when others => assert opened='0' report "Chyba v sedmem vektoru - LUR" severity error;
    end case;
end loop;
reset <= '1'; wait for 3*CLKP; reset <='0'; wait for CLKP;

--v8
for i in 1 to 3 loop
    if v8(2*i-1 to 2*i) = "00" then up <= '1'; end if;
    if v8(2*i-1 to 2*i) = "01" then right <= '1'; end if;
    if v8(2*i-1 to 2*i) = "10" then down <= '1'; end if;
    if v8(2*i-1 to 2*i) = "11" then left <= '1'; end if;
    wait for 300*CLKP;
    up <= '0'; right <= '0'; down <= '0'; left <= '0';
    wait for 100*CLKP;
    case v6(1 to 2) is
        when "00" => assert opened='0' report "Chyba v osmem vektoru - URD" severity error;
        when "01" => assert opened='0' report "Chyba v osmem vektoru - RDL" severity error;
        when "10" => assert opened='0' report "Chyba v osmem vektoru - DLU" severity error;
        when others => assert opened='0' report "Chyba v osmem vektoru - LUR" severity error;
    end case;
end loop;
reset <= '1'; wait for 3*CLKP; reset <='0'; wait for CLKP;

assert false report "Konec testu" severity failure;
end process;

end Behavioral;
