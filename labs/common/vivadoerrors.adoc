:imagesdir: ../../media/labs/common

:mywidth: 600
:mywidth2x: 1200

:toc:
:toclevels: 2

:sectnums:
:sectnumlevels: 1

= Časté chyby v prostředí Vivado a jejich řešení

[NOTE]
Některé z chyb úzce souvisí s používáním síťového disku X: nebo jsou poměrně vzácné, tudíž je obtížné je zopakovat v domácích podmínkách a získat přesný text/screenshot chybového hlášení. Pokud vám tedy nastane některá ze zde zmíněných chyb, která u sebe přesný text/screenshot nemá, budu rád, když mi jej zašlete na mail mailto:martin.kohlik@fit.cvut.cz[martin.kohlik@fit.cvut.cz]. Stejně tak budu rád za zpětnou vazbu, pokud by některé řešení nefungovalo, nebo neobsahovalo všechny kroky. Díky :-)

== Nelze dokončit syntézu – Chyba "Unable to create directory ..."

=== Příčina: Neznámá

=== Řešení: Resetování syntézy a její opětovné spuštění

V levé části _Flow Navigator_ klikněte pravým tlačítkem na volbu _Synthesis_ -> _Run Synthesis_ a v menu vyberte _Reset Synthesis Run_.

image:screenerrorresetsynthesis.png[Reset syntézy - Menu,link={imagesdir}/screenerrorresetsynthesis.png]

Potvrďte reset syntézy.

image:screenerrorresetsynthesis2.png[Reset syntézy - Potvrzení,link={imagesdir}/screenerrorresetsynthesis2.png]

Spusťte znovu syntézu jako obvykle, měla by projít bez chyb.

== Varování při otevírání syntetizovaného návrhu (přiřazování pinů) – "... 'set_property' expects at least one object ..." 

image:screenerroroldportswarning.png[Zastaralé porty/piny - Varování,link={imagesdir}/screenerroroldportswarning.png]

[NOTE]
Při ignorování tohoto varování dojde velmi pravděpodobně v pozdějších fázích překladu k chybám, nebo nebude návrh správně fungovat na vývojovém kitu.

=== Příčina: Změna/přejmenování portu ve schématu po dokončení přiřazování pinů

Nastává také po změně hlavního (top) schématu poté, co bylo původní schéma implementováno na vývojový kit. V takovém případě také nejspíše došlo ke změně jmen portů schématu.

=== Řešení: Ruční odstranění starých portů a pinů ze souboru .xdc

Na kartě _Sources_ v prostřední části rozbalte "adresář" _Constraints_, ve kterém (dvojklikem) otevřete soubor _[jmeno].xdc_.

image:screenerroroldportsxdc.png[Zastaralé porty/piny - otevření .xdc souboru,link={imagesdir}/screenerroroldportsxdc.png]

Obsah souboru .xdc můžete buď vymazat a prázdný soubor poté uložit, nebo se můžete pokusit najít řádky s neaktuálními porty a soubor opravit. Příklad na následujícím obrázku obsahuje jeden přejmenovaný port (_INCREMENT_ ve schématu, _INC_ v .xdc souboru). V takovém případě stačí přejmenovat port ve schématu, nebo upravit zvýrazněné řádky v .xdc souboru. Pokud si však nejste jisti, tak raději obsah souboru vymažte úplně.

image:screenerroroldportsschemeandxdc.png[Zastaralé porty/piny - úpravy .xdc souboru,link={imagesdir}/screenerroroldportsschemeandxdc.png]

Po úpravách zavřete .xdc soubor a znovu spusťte syntézu. Při otevření okna s přiřazováním pinů by se už žádné warningy objevit neměly. Zkontrolujte však přiřazení FPGA pinů a chybějící piny doplňte.

== Nelze dokončit generování bitstreamu – Chyba "Bitstream Generation failed."

image:screenerrorbitstreamfailed.png[Chyba nastavení pinů,link={imagesdir}/screenerrorbitstreamfailed.png]

=== Příčina: Chyba při přiřazování pinů

=== Řešení: Kontrola nastavení pinů a sloupce _I/O Std_

Zkontrolujte, zda máte u všech portů správně nastavené piny FPGA a zda jste u všech portů ve sloupci _I/O Std_ nastavili hodnotu _LVCMOS33_.

////
[#outdatedreference]
== Nelze pracovat s IP jádrem vygenerovaným ze schématu z předchozího projektu – Chyba "??" (neznámá, snad obsahuje výrazy ... out-of-date ... ... black box ...)

=== Příčina: Zastaralá reference na IP blok (nebo bug?)

=== Řešení: Aktualizace reference, případně restart Vivada

Zkuste nejprve aktualizovat reference na IP bloky.

Otevřete schéma a ve žlutém pruhu v horní části klikněte na _Show IP Status_ (žlutý pruh může obsahovat i jiné odkazy typu _... update ..._, _... refresh ..._, které by měly problém vyřešit bez dalších kroků??).

Ve spodní části by se měla otevřít karta _IP Status_ a v její dolní části tlačítko _Upgrade Selected_. Klikněte na _Upgrade Selected_ a potvrďte, že chcete upgrade provést.

image:screenerroroutdatedreference.png[Aktualizace reference na IP bloky - Spuštění,link={imagesdir}/screenerroroutdatedreference.png,width={mywidth}]

image:screenerrorupgradeip.png[Aktualizace reference na IP bloky - Potvrzení,link={imagesdir}/screenerrorupgradeip.png]

Případná další okna zavřete. Po dokončení by měl zmizet žlutý pruh v horní části schématu a IP jádra by měla být aktuální. Pro jistotu klikněte na _Rerun_ na kartě _IP status_ ve spodní části.

image:screenerroroutdatedreference2.png[Aktualizace reference na IP bloky - Dokončení,link={imagesdir}/screenerroroutdatedreference2.png,width={mywidth}]

Po kliknutí na _Rerun_ by měl zmizet žlutý pruh a chybové hlášky i na kartě _IP status_.

image:screenerroroutdatedreference3.png[Aktualizace reference na IP bloky - Výsledek,link={imagesdir}/screenerroroutdatedreference3.png]

Pokud tato aktualizace nezabrala, nebo nelze provést, pak zkuste restartovat Vivado.
////

== Ztracené obrázky ze základních hradel z lib_SAP; Vivado spadne při otevírání schématu (block diagramu)

=== Příčina: Knihovna lib_SAP byla přesunuta do jiného adresáře

Nejčastěji vzniká v případě, že přenášíte projekt z domácího PC/notebooku na školní PC nebo obráceně.

=== Řešení: Opětovné přidání knihovny lib_SAP ze správného adresáře

Odkaz na neaktuální adresář můžete v projektu ponechat (zvlášť v případě, kdy plánujete přenášet projekt mezi domácím PC/notebookem na školním PC opakovaně). V takovém případě se sice mohou zobrazit varování, že je jeden z adresářů prázdný, vše by ale mělo fungovat bez problémů.

//Po přidání správného adresáře buď proveďte aktualizaci jako v návodu u <<#outdatedreference,Nelze pracovat s IP jádrem vygenerovaným ze schématu z předchozího projektu>>, nebo restartujte Vivado.

== Nelze vytvořit projekt, nelze otevřít projekt – Chyba "Error encountered during project creation ..."

image:screenerrornewproject.png[Chyba při vytváření projektu,link={imagesdir}/screenerrornewproject.png]

=== Příčina: Chyba v konfiguraci Vivada (bug?)

=== Řešení: Vymazání adresáře s konfigurací Vivada

Ukončete program Vivado, zazálohujte (pro jistotu) obsah adresáře "c:\Users\[vas_login]\AppData\Roaming\Xilinx\Vivado\" a obsah adresáře poté vymažte. Po spuštění Vivada už by mělo jít s projekty pracovat normálně.

== Nelze provést syntézu pro žádný prvek (ani pro prvky z lib_SAP) – Chyba "Submodule runs failed."

Platné pouze v případě, že selžou úplně všechny prvky. Pokud některé fungují (např. ty z lib_SAP) a jiné ne (ty, které jste dělali sami), pak to bude nejspíš jiný problém.

image:screenerrorsubmodulefailed.png[Submodule runs failed,link={imagesdir}/screenerrorsubmodulefailed.png]

=== Příčina: Cizí znaky (azbuka) z názvu počítače

=== Řešení: Přejmenování názvu počítače (odebrání cizích znaků)

Pro Win10 je návod zde: https://support.microsoft.com/en-us/windows/rename-your-windows-10-pc-750bc75d-8ff8-e99a-b9dc-04dff566ae74


== Náhodné pády Vivada (pod některými distribucemi Linuxu)

=== Příčina: Neznámá (bug ve Vivadu, nepodporovaná distribuce Linuxu)

=== Řešení: Přepsání jednoho ze souborů v instalaci Vivada

Nová verze souboru a přesné umístění ja k dispozici na https://forums.xilinx.com/t5/Design-Entry/Unexpected-error-has-occured-11-Vivado-2018-2/td-p/870547.