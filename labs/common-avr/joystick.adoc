:imagesdir: ../../media/labs/common-avr
:sem05dir: ../../seminars/05
:icons: font

:toc:
:toclevels: 2
:sectnums:
:sectnumlevels: 5

= Práce s joystickem

Na přípravku Butterfly je 5směrný joystick, jehož pozice se mapuje na vstupně/výstupní (I/O) registry _PINB_ a _PINE_ dle následující tabulky:

[options="header", options="autowidth"] 
|===
| Směr          | Registr | Pozice | Maska
| Vlevo         | PINE    | 2      | 0b00000100
| Vpravo        | PINE    | 3      | 0b00001000
| Dolu          | PINB    | 7      | 0b10000000
| Nahoru        | PINB    | 6      | 0b01000000
| Stisk (enter) | PINB    | 4      | 0b00010000
|===

[WARNING]
Hodnota v registrech _PINB_ a _PINE_ je inverzní. *Aktivní směr je indikován log. 0.*

[NOTE]
"Maska" zde znamená informaci, ve kterém bitu kterého registru se nachází informace o pozici v daném směru. Nic není řečeno o ostatních bitech.

== Základní pravidla pro práci s joystickem

Joystick se v programu zpravidla zpracovává tak, že se pomocí <<subroutine#,podprogramu>> pravidelně čte hodnota registru, kam se jeho hodnota zrcadlí (_PINE_ a _PINB_) a ve chvíli, kdy je detekován nějaký pohyb, tak na něj program zareaguje. Lze použít i <<interrupt#,přerušení>>, ale konkrétní inicializace a použití je k dispozici pouze v link:{imagesdir}/atmega169vl.pdf[referenčním manuálu].

Podobně jako u tlačítek na přípravku Basys3 je nutné vyřešit v kódu dva základní problémy:

. Tlačítko může při stisku a uvolnění "šumět", tj. může chvíli trvat, než se jeho hodnota ustálí a během ustálení se může hodnota několikrát změnit. Zatímco ve Vivadu tento problém řešil blok ___debounce__, v asembleru je nutné napsat program tak, aby stav joysticku s malým časovým odstupem načetl víckrát (alespoň 2x) a pokud se načtené hodnoty neshodují, tak načtení ignoroval a nebo provedl znovu.
. Stisk tlačítka může trvat i několik sekund, za tu dobu se program nejspíš stihne zeptat na stav joysticku vícekrát (řádově tisíckrát i více). Ve Vivadu se tento problém řešil vhodným návrhem automatu nebo přidaným blokem tak, aby se i dlouhý stisk započítal pouze jednou. V asembleru je potřeba program napsat tak, aby na delší stisk reagoval pouze jednou (pokud se v dané situaci výslovně nehodí jiné chování). 

== Nastavení joysticku na AVR Butterfly

Před použitím joysticku je potřeba provést jeho nastavení (nastavením správných hodnot do konfiguračních registrů). Porty _B_ a _E_ a tedy i jejich konfigurační registry jsou sdíleny s dalšími periferiemi. Při inicializaci i při čtení hodnot (viz níže) je třeba na tento fakt brát zřetel, jinak může dojít k nechtěným změnám nastavení ostatních periferií (např. displeje).

Následující kód inicializuje joystick tak, aby bylo možno číst jeho stav:

[source]
----
init_joy:
    ; nastaveni portu E (smer vlevo a vpravo)
    in r17, DDRE         ; <1>
    andi r17, 0b11110011
    in r16, PORTE
    ori r16, 0b00001100
    out DDRE, r17
    out PORTE, r16  
    ldi r16, 0b00000000
    sts DIDR1, r16

    ; nastaveni portu B (smer dolu, nahoru a enter)
    in r17, DDRB
    andi r17, 0b00101111
    in r16, PORTB
    ori r16, 0b11010000
    out DDRB, r17
    out PORTB, r16
ret
----
<1> Kvůli sdílení s ostatními periferiemi jsou některé konfigurační registry nejprve načteny, pak jsou v nich nastaveny/vymazány specifické bity a poté jsou uloženy zpět.

== Příklad správného použití

Následující kód správně inicializuje joystick a v jednoduché smyčce čte hodnotu jednoho směru (_enter_) a vypisuje průběžně na displej znak _E_ (_enter_ je stisknutý), nebo _0_ (není stisknutý).

[WARNING]
Jak už bylo uvedeno výše – porty _B_ a _E_ jsou sdíleny s dalšími periferiemi. Nelze tedy hodnotu načíst a hned ji kontrolovat proti nějaké konstantě (např. nule), protože v ostatních bitech můžou být náhodné hodnoty. *Pokud se toto pravidlo nedodrží, tak nebude program na přípravku fungovat.*

[source]
----
; definice pro nas typ procesoru
.include "m169def.inc"
; podprogramy pro praci s displejem
.org 0x1000
.include "print.inc"

; Zacatek programu - po resetu
.org 0
jmp start

; Zacatek programu - hlavni program
.org 0x100
start:
    ; Inicializace zasobniku
    ldi r16, 0xFF
    out SPL, r16
    ldi r16, 0x04
    out SPH, r16
    ; Inicializace displeje
    call init_disp
    ; Inicializace joysticku
    call init_joy

    ldi r17, 2           ; nastaveni pozice pro vypis stavu

main_loop:               ; hlavni smycka
    call read_joy        ; nacti stav joysticku do r20
    cpi r20, 1           ; podivej se, co je v nem za hodnotu
    brne no_enter        ; neni enter -> preskoc

    ldi r16, 'E'         ; je enter
    jmp show_result
no_enter:
    ldi r16, '0'         ; neni enter

show_result:
    call show_char       ; vypis stav

jmp main_loop


end: jmp end

init_joy:                ; inicializace joysticku
    in r17, DDRE
    andi r17, 0b11110011
    in r16, PORTE
    ori r16, 0b00001100
    out DDRE, r17
    out PORTE, r16  
    ldi r16, 0b00000000
    sts DIDR1, r16
    in r17, DDRB
    andi r17, 0b00101111
    in r16, PORTB
    ori r16, 0b11010000
    out DDRB, r17
    out PORTB, r16
ret

read_joy:                ; ulozi smer joysticku do registru r20
    push r16             ; uklid r16 a r17
    push r17

joy_reread:
    in r16, PINB         ; nacti hodnotu joysticku <1>

    ldi r20, 255         ; chvili cekej <2>
joy_wait: dec r20
    brne joy_wait

    in r17, PINB         ; nacti jeste jednou

    andi r16, 0b00010000 ; vymaskuj ostatni bity <3>
    andi r17, 0b00010000

    cp r16, r17
    brne joy_reread      ; hodnoty se nerovnaly -> nacti znovu

    ldi r20, 0           ; vychozi hodnota - nic neni aktivni
    cpi r16, 0
    brne joy_no_enter    ; hodnota je inverzni -> neni 0 znamena neni aktivni <4>
    ldi r20, 1           ; r20 = 1, kdyz je enter aktivni
joy_no_enter:

   pop r17               ; obnoveni r16 a r17
   pop r16
ret
----
<1> Čtení z I/O registrů _PINB_ a _PINE_ je pomocí instrukce __**in**__. V příkladu není čtení registru _PINE_, protože ukazuje pouze zpracování směru _enter_.
<2> Jednoduchá čekací smyčka je v tomto případě dostačující.
<3> Použitá maska zachovává pouze bit pro směr _enter_, pokud chcete zpracovat více různých směrů najednou, tak je potřeba ji upravit.
<4> Existují instrukce, které se umí ptát na konkrétní bit v registru _r_, nebo v I/O registrech (_PINB_ a _PINE_) a na základě výsledku rovnou skočit. Pokud byste je chtěli použít, tak je najdete v <<instruction-list#,přehledu instrukcí>>.


== Simulace joysticku

Pro dosažení realističtějších podmínek při simulaci joysticku je vhodné dodržovat následující pravidla:

. V panelu joysticku zaskrtněte volbu _ON/OFF Mode_ (viz následující obrázek), která umožní simulaci dlouhého stisku. Zaškrtnutím této volby zůstanou tlačítka směrů po stisknutí "zamáčknutá" až do dalšího stisku, který je uvolní. Pokud volbu nezaškrtnete, tak se tlačítko po provedení jedné instrukce uvolní automaticky. +
image:avr-joystick.png[]
. V panelu displeje aktivujte třetí ikonu (hodiny). Po aktivaci se displej aktualizuje automaticky i za běhu programu. Pokud ikonu neaktivujete, tak se displej spolehlivě aktualizuje pouze při zapauzování programu (např. při breakpointu), ale za běhu programu může vynechávat (a program tak může vypadat na pohled nefukční). +
image:lcd-toolbar-clock.png[]

Výše uvedený příklad můžete s tímto nastavením spustit pomocí volby _Run (F5)_ a při stisku tlačítka pro směr _enter_ v panelu joysticku se automaticky změní i znak na displeji.

Pokud budete simulovat kód, který má reagovat na dlouhý stisk  tlačítka pouze jednou, tak využijte vhodně umístěné breakpointy (breakpoint umístíte/zrušíte na řádek s kurzorem pomocí klávesy _F9_) a spuštějte program pomocí volby _Run (F5)_. Pokud byste program krokovali a aktivovali tlačítka joysticku jen na pár instrukcí, tak hrozí, že na přípravku bude program reagovat na jeden stisk vícekrát.

