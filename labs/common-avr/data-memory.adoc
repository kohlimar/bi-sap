:imagesdir: ../../media/labs/common-avr
:icons: font

:toc:
:toclevels: 2
:sectnums:
:sectnumlevels: 5

= Adresace dat v paměti dat

Adresa paměti dat je 16bitová, ale na rozdíl od <<program-memory#,paměti programu>> je organizovaná po bajtech.

*V paměti dat lze rezervovat příslušný prostor a za běhu programu je přístupná pro zápis i čtení.*

[TIP]
Obsah paměti si v simulátoru AVR Studia můžete prohlédnout pomocí menu _View_ -> _Memory_. V okně si pak vyberete, kterou paměť chcete zobrazit (data/program) a jakým způsobem chcete data zobrazit (8/16bitová organizace, ASCII). Paměť lze zobrazit až po spuštění simulace.

Pro přístup do paměti dat lze použít následující typy adresace:

* Přímá adresace – adresa je součástí instrukce
** Instrukce __**sts**__ a __**lds**__
** Možnost využít návěští
* Nepřímá adresace – přes indexregistry _X_, _Y_, _Z_
** Instrukce __**ld**__, __**st**__
** Indexregistry jsou 16bitové a jsou mapované na běžné _r_ registry, aby s nimi bylo možné pracovat i běžnými instrukcemi:
*** _X_ = (_r27_, _r26_) – pouze pro paměť dat
*** _Y_ = (_r29_, _r28_) – pouze pro paměť dat
*** _Z_ = (_r31_, _r30_) – pro paměť dat i programu
** Všechny indexregistry mají možnost pre-dekrementace (-_X_) a post-inkrementace (_X_+)

Následující příklad obsahuje instrukce pro práci s pamětí dat i programu (detaily o paměti programu naleznete <<program-memory#,zde>>).
[source]
----
.dseg                     ; prepnuti do pameti dat
.org 0x100                ; od adresy 0x100 (adresy 0 - 0x100 nepouzivejte)

prom1: .byte 1            ; rezervovani mista pro 1 bajt <1>
prom2: .byte 10           ; rezervovani mista pro 10 bajtu

;.org 0x106               ; na adrese 0x106 "bydli" prom2, prekladac hlasi konflikt 
                          ;         a nedovoli vice navesti pro jednu pozici pameti <2>
;prom3: .byte 1  

.cseg                     ; prepnuti do pameti programu                
; definice pro nas typ procesoru
.include "m169def.inc"

; Zacatek programu - po resetu
.org 0
jmp start
	
.org 0x100
delka: .db 6              ; definice read-only konstanty v pameti programu (jeden bajt s hodnotou 6) <3>
retez: .db "MUJ RETEZEC",0; retezec zakonceny nulou (nikoli znakem "0") <4>
	

start:
    ldi r20, 50
    sts	0x100, r20        ; ulozi obsah registru do datove pameti na adresu 0x100

    ldi r21, 60
    sts prom1, r21        ; ulozi obsah registru do datove pameti na adresu prom1 (tj. take 0x100) <5>

    lds r18, 0x100        ; nacte obsah registru z datove pameti z adresy 0x100
    lds r19, prom1        ; nacte obsah registru z datove pameti z adresy prom1 (tj. take 0x100)

    ldi r28, 0x06
    ldi r29, 0x01         ; Y = 0x106 <6>
    st Y, r20             ; uloz r20 na adresu, kam ukazuje Y (tj. 0x106)
    st Y+, r21            ; uloz r21 na adresu, kam ukazuje Y a pote inkrementuj Y
    st -Y, r21            ; dekrementuj Y a pote uloz r21 na adresu, kam ukazuje Y

    ld r20, Y             ; nacti r20 z adresy, kam ukazuje Y <7>

    ldi r26, low(prom2)
    ldi r27, high(prom2)  ; X = 0x101


    ldi r30, low(2*retez) ; <8>
    ldi r31, high(2*retez); Z = 0x100

    lpm r16, Z+           ; <9>

end: jmp end
----
<1> _.byte_ je tzv. direktiva pro překladač. Je to pokyn, aby překladač rezervoval zadaný počet bajtů v paměti (dat). Číslo 1 tedy není hodnota, která by se do paměti uložila (na rozdíl od paměti programu u <3>).
<2> Překladač automaticky hlídá konflikty v umístění návěštích. Výrazně tak snižuje pravděpodobnost, že si v datové paměti přepíšete omylem užitečná data.
<3> _.db_ je další direktiva pro překladač. Je to pokyn, aby následující hodnoty vzal jako seznam bajtů a uložil do paměti (programu). Je možno zadat jednotlivé hodnoty bajtů oddělené čárkou, nebo i celý řetězec v uvozovkách.
<4> _retez_ je návěští (ukazatel) prvního znaku řetězce (tj. "N"). Je to tedy konstanta, ze které překladač může vzít spodní bajt (_low(2*retez)_) a pomocí _**ldi**_ ho uložit do registru _r30_. Obdobně horní bajt (_high(2*retez)_) a registr _r31_.
<5> Použití návěští namísto přímého čísla je důrazně doporučeno. Na případné překlepy může totiž upozornit překladač.
<6> Registr _Y_ je namapován na registry _r28_ a _r29_. Jeden z důvodů je právě možnost načtení hodnoty přes dvě instrukce _**ldi**_, protože AVR pracuje s 8bitovými daty. AVR ale obsahuje i instrukce, které lze použít přímo s dvojicí registrů (tj. i s _X_, _Y_ a _Z_) a to jak nahrávací/přesouvací, tak i aritmetické – viz <<instruction-list#,přehled instrukcí>>.
<7> I pro __**ld**__ existují varianty s inkrementací a dekrementací – viz <<instruction-list#,přehled instrukcí>>.
<8> Paměť programu je organizovaná po 16bitových slovech. _retez_ tedy obsahuje adresu 16bitového slova. Aby bylo možno pracovat s jednotlivými bajty, je adresa pro instrukci __**lpm**__ násobená dvěma (detaily viz <<program-memory#,paměť programu>>).
<9> Instrukce _**lpm** r16, Z+_ nahraje jeden bajt (znak) z paměti programu, na který ukazuje _Z_, do registru a zároveň inkrementuje Z. Pro práci s pamětí programu lze použít pouze registr _Z_.

