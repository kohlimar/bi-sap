:imagesdir: ../../media/labs/common-avr

:mywidth: 600
:mywidth2x: 1200
:sectnums:
:sectnumlevels: 5

:toc:
:toclevels: 2


= Popis vývojového kitu AVR Butterfly


Pro práci s asemblerem budeme v rámci předmětu požívat vývojový kit AVR Butterfly (link:https://www.microchip.com/DevelopmentTools/ProductDetails/PartNO/ATAVRBFLY[Stránky výrobce]).

Vývojový kit je založen na link:https://cs.wikipedia.org/wiki/Jedno%C4%8Dipov%C3%BD_po%C4%8D%C3%ADta%C4%8D[mikropočítači] ATmega169. Celý referenční manuál je k dispozici link:{imagesdir}/avr-butterfly-eval-kit.pdf[zde], níže jsou uvedeny informace relevantní k tomuto předmětu.

== Fotografie vývojového kitu

image:butterfly_photo.png[[Vývojový kit AVR Butterfly - převzato z letáku výrobce (Pro zvětšení klikněte),link={imagesdir}/butterfly_photo.png]

== Seznam periferií

V rámci předmětu budeme využívat:

* Displej se 6 pozicemi na alfanumerické znaky (po 14 segmentech)
* Joystick (switch) – 5 směrů (nahoru, dolů, doleva, doprava, kolmý stisk (enter))
* 32 768 Hz oscilátor (pro přerušení u závěrečné úlohy)

Kit dále obsahuje:

* Napájecí baterii – *pokud přípravek nepoužíváte, vždy jej resetujte tlačítkem umístěným vpravo nahoře na kovové základně*. Po resetu přechází přípravek do úsporného módu. Pokud přípravek nejde naprogramovat, může to být právě vybitou baterií.
* Bzučák (buzzer)
* Čidlo osvětlení (LDR)
* Čidlo teploty (NTC)

Kit je ve škole umístěn na kovové základně s resetujícím tlačítkem umístěným vpravo nahoře a sériovým portem.

== Mikropočítač AVR ATmega169

AVR firmy Atmel je rodina procesorů, které mají na jednom čipu integrován procesor, paměť programu, paměť dat a poměrně rozsáhlou sadu periferií (vstupně-výstupní brány, čítače, časovače, převodníky, komunikační rozhraní apod.). Takovým součástkám říkáme jednočipové mikropočítače (protože na jednom čipu je skutečně plně funkční počítač, i když možná jednoduchý).

Konkrétní mikropočítač AVR, se kterým budeme pracovat, se jmenuje *ATmega169* a obsahuje:

* Univerzální registry – 32 osmibitových registrů (R0 až R31)
* Posledních šest z nich tvoří po dvojicích tři indexregistry – X = R27:R26, Y = R29:R28, Z = R31:R30
** Slouží k adresování přístupu do paměti
* Speciální registry
** Programový čítač – 16bitový (Program Counter - PC)
** Ukazatel na vrchol zásobníku – 16bitový (Stack Pointer - SP)
** Registr příznaků – 8bitový (Status Register - SREG)
* Paměť dat: 1 KB, organizace 1K x 8 bitů, čtení i zápis
* Paměť programu: 16KB, organizace 8K x 16 bitů, pouze čtení (zápis speciálním způsobem)
* Z periferií zejména řadič displeje (LCD)
* Vnitřní zdroj hodinového kmitočtu