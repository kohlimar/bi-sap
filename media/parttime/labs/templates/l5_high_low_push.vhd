library IEEE;
use ieee.std_logic_1164.all;

entity high_low_push is
    port(
        clk       : in  std_logic;
        rst       : in  std_logic;
        btn       : in  std_logic;
        value     : out std_logic;
        btn_event : out std_logic
    );
end high_low_push;

architecture behavioral of high_low_push is

-----------------------------------------------------------
-- Kdo si chce hrat s casovanim, zde ma moznost------------
-----------------------------------------------------------
    constant clk_freq       :   integer   := 100000000;  -- clk_freq je nastaven na 1 sekundu - nemenit
    constant counter_TOP    :   integer   := clk_freq/2; -- stisk delsi nez counter_TOP je bran jako dlouhy
-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------

    type execution_stage is (s0,s1,s2);

    signal state, next_state           :  execution_stage;
    signal sig_counter                 :  integer range 0 to counter_TOP;
    signal sig_rst_counter, sig_count  :  std_logic;

    ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
    ATTRIBUTE X_INTERFACE_PARAMETER OF rst: SIGNAL IS "XIL_INTERFACENAME Reset, POLARITY ACTIVE_HIGH";

begin

    proc_counter: process (clk, rst, sig_count, sig_rst_counter)
    begin
        if (rst='1' or sig_rst_counter='1') then
            sig_counter <=  0;
        elsif (clk='1' and clk'event) then
            if (sig_counter < counter_top and sig_count='1') then
                sig_counter <= sig_counter + 1;
            end if;
        end if;
    end process;

    proc_fsm: process (clk, rst)
    begin
        if (rst='1') then
            state   <=  s0;
        elsif (clk='1' and clk'event) then
            state   <=  next_state;
        end if;
    end process;

    proc_fsm_trans: process (state, btn)
    begin
        case state is
            when s0 =>
                next_state <=  s0;
                if (btn='1') then
                    next_state <= s1;
                end if;
            when s1 =>
                next_state <= s1;
                if (btn='0') then
                    next_state <= s2;
                end if;
            when s2 =>
                next_state <= s0;
            when others =>
                next_state <= s0;
        end case;
    end process;

    proc_fsm_output: process (state, sig_counter)
    begin
        sig_count       <=  '0';
        sig_rst_counter <=  '0';
        value           <=  '0';
        btn_event       <=  '0';

        case state is
            when s0  =>
                sig_rst_counter <=  '1';
            when s1  =>
                sig_count       <=  '1';
            when s2  =>
                btn_event       <=  '1';
                if (sig_counter < counter_top) then
                    value       <=  '0';
                else
                    value       <=  '1';
                end if;
            when others =>
        end case;
    end process;

end;
