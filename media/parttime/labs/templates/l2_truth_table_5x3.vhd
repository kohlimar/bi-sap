library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;

-- Ports description - inputs a, b, c, d, e, outputs f2, f1, f0
-- definice vstupu a vystupu - vstupy a, b, c, d, e, vystupy f2, f1, f0
entity truth_table_5x3 is
    port(
        e  : in  std_logic;
        d  : in  std_logic;
        c  : in  std_logic;
        b  : in  std_logic;
        a  : in  std_logic;
        f2 : out std_logic;
        f1 : out std_logic;
        f0 : out std_logic
    );
end truth_table_5x3;

architecture behavioral of truth_table_5x3 is

-- definition of the truth-table type - has 32 outputs (2^5 = 32) of standard logic type (0, 1)
-- definice typu pravdivostni tabulka - obsahuje 32 vystupu (2^5 = 32) typu standardni logika (0, 1)
    type truth_table is array (0 to 31) of std_logic_vector(2 downto 0);

-- definition of our functions f2, f1, f0(e, d, c, b, a) by a truth table
-- definice nasich funkci f2, f1, f0(e, d, c, b, a) pravdivostni tabulkou
    constant our_function : truth_table := truth_table'(

-- do not change the order of the inputs a, b, c, d, e in the header - it is just a comment
-- zmena poradi vstupu a, b, c, d, e v zahlavi nema smysl - jedna se pouze o komentar

-- ------------------------------------------------------------------------------------
-- | functional value f2, f1, f0(e, d, c, b, a) | index |  e  |  d  |  c  |  b  |  a  |
-- | funkcni hodnota f2, f1, f0(e, d, c, b, a)  | index |  e  |  d  |  c  |  b  |  a  |
-- ------------------------------------------------------------------------------------
                 "000",                      -- |   0   |  0  |  0  |  0  |  0  |  0  |
                 "000",                      -- |   1   |  0  |  0  |  0  |  0  |  1  |
                 "000",                      -- |   2   |  0  |  0  |  0  |  1  |  0  |
                 "000",                      -- |   3   |  0  |  0  |  0  |  1  |  1  |
                 "000",                      -- |   4   |  0  |  0  |  1  |  0  |  0  |
                 "000",                      -- |   5   |  0  |  0  |  1  |  0  |  1  |
                 "000",                      -- |   6   |  0  |  0  |  1  |  1  |  0  |
                 "000",                      -- |   7   |  0  |  0  |  1  |  1  |  1  |
                 "000",                      -- |   8   |  0  |  1  |  0  |  0  |  0  |
                 "000",                      -- |   9   |  0  |  1  |  0  |  0  |  1  |
                 "000",                      -- |  10   |  0  |  1  |  0  |  1  |  0  |
                 "000",                      -- |  11   |  0  |  1  |  0  |  1  |  1  |
                 "000",                      -- |  12   |  0  |  1  |  1  |  0  |  0  |
                 "000",                      -- |  13   |  0  |  1  |  1  |  0  |  1  |
                 "000",                      -- |  14   |  0  |  1  |  1  |  1  |  0  |
                 "000",                      -- |  15   |  0  |  1  |  1  |  1  |  1  |
                 "000",                      -- |  16   |  1  |  0  |  0  |  0  |  0  |
                 "000",                      -- |  17   |  1  |  0  |  0  |  0  |  1  |
                 "000",                      -- |  18   |  1  |  0  |  0  |  1  |  0  |
                 "000",                      -- |  19   |  1  |  0  |  0  |  1  |  1  |
                 "000",                      -- |  20   |  1  |  0  |  1  |  0  |  0  |
                 "000",                      -- |  21   |  1  |  0  |  1  |  0  |  1  |
                 "000",                      -- |  22   |  1  |  0  |  1  |  1  |  0  |
                 "000",                      -- |  23   |  1  |  0  |  1  |  1  |  1  |
                 "000",                      -- |  24   |  1  |  1  |  0  |  0  |  0  |
                 "000",                      -- |  25   |  1  |  1  |  0  |  0  |  1  |
                 "000",                      -- |  26   |  1  |  1  |  0  |  1  |  0  |
                 "000",                      -- |  27   |  1  |  1  |  0  |  1  |  1  |
                 "000",                      -- |  28   |  1  |  1  |  1  |  0  |  0  |
                 "000",                      -- |  29   |  1  |  1  |  1  |  0  |  1  |
                 "000",                      -- |  30   |  1  |  1  |  1  |  1  |  0  |
                 "000"                       -- |  31   |  1  |  1  |  1  |  1  |  1  |
-- ------------------------------------------------------------------------------------
    );

    signal address : std_logic_vector(4 downto 0);
begin
    address(4) <= e;
    address(3) <= d;
    address(2) <= c;
    address(1) <= b;
    address(0) <= a;

    f0 <= our_function(conv_integer(address(4 downto 0)))(0);
    f1 <= our_function(conv_integer(address(4 downto 0)))(1);
    f2 <= our_function(conv_integer(address(4 downto 0)))(2);

end;
