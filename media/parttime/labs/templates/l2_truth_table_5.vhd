library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
 
-- Ports description - inputs a, b, c, d, e, output f
-- definice vstupu a vystupu - vstupy a, b, c, d, e, vystup f
entity truth_table_5 is
    port(
        e : in  std_logic;
        d : in  std_logic;
        c : in  std_logic;
        b : in  std_logic;
        a : in  std_logic;
        f : out std_logic
    );
end truth_table_5;

architecture behavioral of truth_table_5 is

-- definition of the truth-table type - has 32 outputs (2^5 = 32) of standard logic type (0, 1)
-- definice typu pravdivostni tabulka - obsahuje 32 vystupu (2^5 = 32) typu standardni logika (0, 1)
    type truth_table is array (0 to 31) of std_logic;

-- definition of our function f(e, d, c, b, a) by a truth table
-- definice nasi funkce f(e, d, c, b, a) pravdivostni tabulkou
    constant our_function : truth_table := truth_table'(

-- do not change the order of the inputs a, b, c, d, e in the header - it is just a comment
-- zmena poradi vstupu a, b, c, d, e v zahlavi nema smysl - jedna se pouze o komentar

-- ---------------------------------------------------------------------------
-- | functional value f(e, d, c, b, a) | index |  e  |  d  |  c  |  b  |  a  |
-- | funkcni hodnota f(e, d, c, b, a)  | index |  e  |  d  |  c  |  b  |  a  |
-- ---------------------------------------------------------------------------
                 '0',               -- |   0   |  0  |  0  |  0  |  0  |  0  |
                 '1',               -- |   1   |  0  |  0  |  0  |  0  |  1  |
                 '0',               -- |   2   |  0  |  0  |  0  |  1  |  0  |
                 '1',               -- |   3   |  0  |  0  |  0  |  1  |  1  |
                 '0',               -- |   4   |  0  |  0  |  1  |  0  |  0  |
                 '1',               -- |   5   |  0  |  0  |  1  |  0  |  1  |
                 '0',               -- |   6   |  0  |  0  |  1  |  1  |  0  |
                 '1',               -- |   7   |  0  |  0  |  1  |  1  |  1  |
                 '0',               -- |   8   |  0  |  1  |  0  |  0  |  0  |
                 '1',               -- |   9   |  0  |  1  |  0  |  0  |  1  |
                 '0',               -- |  10   |  0  |  1  |  0  |  1  |  0  |
                 '1',               -- |  11   |  0  |  1  |  0  |  1  |  1  |
                 '0',               -- |  12   |  0  |  1  |  1  |  0  |  0  |
                 '1',               -- |  13   |  0  |  1  |  1  |  0  |  1  |
                 '0',               -- |  14   |  0  |  1  |  1  |  1  |  0  |
                 '1',               -- |  15   |  0  |  1  |  1  |  1  |  1  |
                 '0',               -- |  16   |  1  |  0  |  0  |  0  |  0  |
                 '1',               -- |  17   |  1  |  0  |  0  |  0  |  1  |
                 '0',               -- |  18   |  1  |  0  |  0  |  1  |  0  |
                 '1',               -- |  19   |  1  |  0  |  0  |  1  |  1  |
                 '0',               -- |  20   |  1  |  0  |  1  |  0  |  0  |
                 '1',               -- |  21   |  1  |  0  |  1  |  0  |  1  |
                 '0',               -- |  22   |  1  |  0  |  1  |  1  |  0  |
                 '1',               -- |  23   |  1  |  0  |  1  |  1  |  1  |
                 '0',               -- |  24   |  1  |  1  |  0  |  0  |  0  |
                 '1',               -- |  25   |  1  |  1  |  0  |  0  |  1  |
                 '0',               -- |  26   |  1  |  1  |  0  |  1  |  0  |
                 '1',               -- |  27   |  1  |  1  |  0  |  1  |  1  |
                 '0',               -- |  28   |  1  |  1  |  1  |  0  |  0  |
                 '1',               -- |  29   |  1  |  1  |  1  |  0  |  1  |
                 '0',               -- |  30   |  1  |  1  |  1  |  1  |  0  |
                 '1'                -- |  31   |  1  |  1  |  1  |  1  |  1  |
-- ---------------------------------------------------------------------------
    );

    signal address : std_logic_vector(4 downto 0);

begin
    address(4) <= e;
    address(3) <= d;
    address(2) <= c;
    address(1) <= b;
    address(0) <= a;

    f <= our_function(conv_integer(address(4 downto 0)));

end;
