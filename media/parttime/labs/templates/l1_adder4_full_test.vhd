library STD;
use STD.textio.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;

entity adder4_full_test is
end adder4_full_test;

architecture behavioral of adder4_full_test is

    signal cin  : std_logic := '0';
    signal a, b : std_logic_vector(3 downto 0) := "0000";

    signal cout : std_logic;
    signal s    : std_logic_vector(3 downto 0);

    signal no_of_errors : integer := 0;
    constant DELAY      : time := (990 ns) / 512 / 4;
    signal terminate    : boolean;

    -- poznamka pro cvicici: deklarace komponenty neni nutna - vyber testovaneho obvodu je az u konkretni instance

begin

    -- definition of tested entity and its inputs and outputs
    -- inputs:  a0, a1, a2, a3, b0, b1, b2, b3, cin
    -- outputs: s0, s1, s2, s3, cout
    -- ATTENTION!
    -- The entity name (entity work.adder_wrapper) must be equal to the tested component name
    -- In this case: adder_wrapper
    -- The input and output names (left part before arrow) must be equal to the names in the tested design

    -- definice vstupu a vystupu testovane komponenty
    -- vstupy:  a0, a1, a2, a3, b0, b1, b2, b3, cin
    -- vystupy: s0, s1, s2, s3, cout
    -- POZOR!
    -- nazev entity (entity work.adder_wrapper) musi byt stejny jako nazev testovane entity
    -- v tomto pripade: adder_wrapper
    -- vstupy a vystupy (v leve casti pred sipkou) musi byt stejne jako popisy vstupu a vystupu testovane entity

    uut: entity work.adder_wrapper
    port map(
        cin  => cin,
        a0   => a(0),
        a1   => a(1),
        a2   => a(2),
        a3   => a(3),
        b0   => b(0),
        b1   => b(1),
        b2   => b(2),
        b3   => b(3),
        s0   => s(0),
        s1   => s(1),
        s2   => s(2),
        s3   => s(3),
        cout => cout
   );

    buzzer: process
        variable file_line : line;
    begin
        wait for 1000 ns;
        if (not terminate) then
            write(file_line, string'("-->>> Simulation paused - press Run-all to continue! <<<--"));
            writeline(output, file_line);
        end if;
        wait;
    end process;

    stim_proc: process
        variable file_line : line;
    begin
        terminate <= false;

        -- zacatek simulace
        write(file_line, string'("### Simulation start ###"));
        writeline(output, file_line);

        cin <= '0';
        for i in 0 to 15 loop
            for j in 0 to 15 loop
                a <= std_logic_vector(to_unsigned(i, 4));
                b <= std_logic_vector(to_unsigned(j, 4));
                wait for DELAY;

                if ((('0' & a) + ('0' & b) + cin) /= (cout & s)) then
                    write(file_line, string'("Error in adding: / Chyba pri scitani: CIN="));
                    write(file_line, to_bit(cin));
                    write(file_line, string'(", A+B="));
                    write(file_line, i);
                    write(file_line, string'("+"));
                    write(file_line, j);
                    writeline(output, file_line);
                    no_of_errors <= no_of_errors + 1;
                end if;

                wait for 3*DELAY;
            end loop;
        end loop;

        cin <= '1';
        for i in 0 to 15 loop
            for j in 0 to 15 loop
                a <= std_logic_vector(to_unsigned(i, 4));
                b <= std_logic_vector(to_unsigned(j, 4));
                wait for DELAY;

                if ((('0' & a) + ('0' & b) + cin) /= (cout & s)) then
                    write(file_line, string'("Error in adding: / Chyba pri scitani: CIN="));
                    write(file_line, to_bit(cin));
                    write(file_line, string'(", A+B="));
                    write(file_line, i);
                    write(file_line, string'("+"));
                    write(file_line, j);
                    writeline(output, file_line);
                    no_of_errors <= no_of_errors + 1;
                end if;

                wait for 3*DELAY;
            end loop;
        end loop;

        -- konec simulace
        write(file_line, string'("### Total number of errors = "));
        write(file_line, no_of_errors);
        writeline(output, file_line);

        write(file_line, string'("### Simulation finished ###"));
        writeline(output, file_line);

        terminate <= true;

        wait;

   end process;

end;
