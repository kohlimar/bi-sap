library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;

-- Ports description - inputs a, b, c, d, outputs f2, f1, f0
-- definice vstupu a vystupu - vstupy a, b, c, d, vystupy f2, f1, f0
entity truth_table_4x3 is
    port(
        d  : in  std_logic;
        c  : in  std_logic;
        b  : in  std_logic;
        a  : in  std_logic;
        f2 : out std_logic;
        f1 : out std_logic;
        f0 : out std_logic
    );
end truth_table_4x3;

architecture behavioral of truth_table_4x3 is

-- definition of the truth-table type - has 16 outputs (2^4 = 16) of standard logic type (0, 1)
-- definice typu pravdivostni tabulka - obsahuje 16 vystupu (2^4 = 16) typu standardni logika (0, 1)
    type truth_table is array (0 to 15) of std_logic_vector(2 downto 0);

-- definition of our functions f2, f1, f0(d, c, b, a) by a truth table
-- definice nasich funkci f2, f1, f0(d, c, b, a) pravdivostni tabulkou
    constant our_function : truth_table := truth_table'(

-- do not change the order of the inputs a, b, c, d in the header - it is just a comment
-- zmena poradi vstupu a, b, c, d v zahlavi nema smysl - jedna se pouze o komentar

-- ---------------------------------------------------------------------------
-- | functional value f2, f1, f0(d, c, b, a) | index |  d  |  c  |  b  |  a  |
-- | funkcni hodnota f2, f1, f0(d, c, b, a)  | index |  d  |  c  |  b  |  a  |
-- ---------------------------------------------------------------------------
                 "000",                   -- |   0   |  0  |  0  |  0  |  0  |
                 "000",                   -- |   1   |  0  |  0  |  0  |  1  |
                 "000",                   -- |   2   |  0  |  0  |  1  |  0  |
                 "000",                   -- |   3   |  0  |  0  |  1  |  1  |
                 "000",                   -- |   4   |  0  |  1  |  0  |  0  |
                 "000",                   -- |   5   |  0  |  1  |  0  |  1  |
                 "000",                   -- |   6   |  0  |  1  |  1  |  0  |
                 "000",                   -- |   7   |  0  |  1  |  1  |  1  |
                 "000",                   -- |   8   |  1  |  0  |  0  |  0  |
                 "000",                   -- |   9   |  1  |  0  |  0  |  1  |
                 "000",                   -- |  10   |  1  |  0  |  1  |  0  |
                 "000",                   -- |  11   |  1  |  0  |  1  |  1  |
                 "000",                   -- |  12   |  1  |  1  |  0  |  0  |
                 "000",                   -- |  13   |  1  |  1  |  0  |  1  |
                 "000",                   -- |  14   |  1  |  1  |  1  |  0  |
                 "000"                    -- |  15   |  1  |  1  |  1  |  1  |
-- ---------------------------------------------------------------------------
    );

    signal address : std_logic_vector(3 downto 0);

begin
    address(3) <= d;
    address(2) <= c;
    address(1) <= b;
    address(0) <= a;

    f0 <= our_function(conv_integer(address(3 downto 0)))(0);
    f1 <= our_function(conv_integer(address(3 downto 0)))(1);
    f2 <= our_function(conv_integer(address(3 downto 0)))(2);

end;
