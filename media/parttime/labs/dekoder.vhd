----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:23:24 03/16/2012 
-- Design Name: 
-- Module Name:    dekoder - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: Tento material slouzi POUZE pro studenty dalkoveho studia
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity dekoder is
	port(
		a   : in  STD_LOGIC;
		b   : in  STD_LOGIC;
		c   : in  STD_LOGIC;
		d   : in  STD_LOGIC;
		f_a : out STD_LOGIC;
		f_b : out STD_LOGIC;
		f_c : out STD_LOGIC;
		f_d : out STD_LOGIC;
		f_e : out STD_LOGIC;
		f_f : out STD_LOGIC;
		f_g : out STD_LOGIC
	);
end dekoder;

architecture Behavioral of dekoder is
begin
	
	f_a <= (not(((not a) and (not c)) or (b and (not d)) or (b and c) or ((not a) and (not b) and d) or (a and c and (not d)) or ((not b) and (not c) and d)));
	f_b <= (not(((not c) and (not d)) or ((not a) and (not c)) or (a and (not b) and d) or ((not a) and (not b) and (not d)) or (a and b and (not d))));
	f_c <= (not((a and (not b)) or ((c) and (not d)) or ((not c) and d) or ((not b) and (not d)) or (a and (not d))));
	f_d <= -- sem doplnte funkci;
	f_e <= -- sem doplnte funkci;
	f_f <= (not(((not a) and (not b)) or ((not c) and d) or (b and d) or ((not b) and c and (not d)) or ((not a) and b and  c)));
	f_g <= (not(((not a) and b) or (b and (not c)) or ((not c) and d) or (a and d) or ((not b) and c and (not d))));

end Behavioral;