library STD;
use STD.textio.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_textio.all;

entity forbes_test is
    generic(
        C_COUNTER_DELAY   : integer := 8 -- zpozdeni signalu Shift - pocet taktu
    );
    port(
        Reset             : out std_logic;
        Clock             : out std_logic;
        Shift             : out std_logic;
        Roll              : out std_logic;

        Shift_init        : in std_logic;
        Count_en          : in std_logic;

        A, B, C, D        : in std_logic;
        Correct_Val       : out std_logic_vector(3 downto 0);
        LFSR_Val          : out std_logic_vector(3 downto 0);
        No_of_errors      : out std_logic_vector(7 downto 0);
        No_of_LFSR_errors : out std_logic_vector(7 downto 0)
    );
end forbes_test;

architecture behavioral of forbes_test is

    signal clk : std_logic := '1';
    signal rst : std_logic := '0';

    signal tb_val  : std_logic_vector(3 downto 0) := "0000";
    signal out_val : std_logic_vector(3 downto 0) := "0000";

    signal counter_value : integer := 0;
    signal shift_tb      : std_logic := '0';

    constant CLK_PERIOD : time := 4 ns;
    signal terminate    : boolean;

    signal no_of_LFSR_errors_tb : integer := 0;

    ATTRIBUTE X_INTERFACE_INFO : STRING;
    ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
    ATTRIBUTE X_INTERFACE_PARAMETER OF Reset: SIGNAL IS "XIL_INTERFACENAME Reset, POLARITY ACTIVE_HIGH";
    ATTRIBUTE X_INTERFACE_INFO OF Reset: SIGNAL IS "xilinx.com:signal:reset:1.0 Reset RST";
    ATTRIBUTE X_INTERFACE_PARAMETER OF Clock: SIGNAL IS "XIL_INTERFACENAME Clock, ASSOCIATED_RESET Reset, FREQ_HZ 100000000";
    ATTRIBUTE X_INTERFACE_INFO OF Clock: SIGNAL IS "xilinx.com:signal:clock:1.0 Clock CLK";

begin

    clock <= clk;
    reset <= rst;
    correct_val <= tb_val;
    out_val <= d & c & b & a;
    shift <= shift_tb;
    lfsr_val <= out_val;
    no_of_LFSR_errors <= std_logic_vector(to_unsigned(no_of_LFSR_errors_tb, 8));

    buzzer: process
        variable file_line : line;
    begin
        wait for 1000 ns;
        if (not terminate) then
            write(file_line, string'("-->>> Simulation paused - press Run-all to continue! <<<--"));
            writeline(output, file_line);
        end if;
        wait;
    end process;

    process
    begin
        clk <= not clk;
        wait for CLK_PERIOD/2;
        if (terminate) then
            wait;
        end if;
    end process;

    -- LFSR reference
    process (clk, rst)
    begin
        if rst = '1' then
            tb_val <= (others => '0');
        elsif (clk'event and clk = '1') then
            if shift_tb = '1' then
                tb_val <= tb_val(2 downto 0) & (shift_init or (tb_val(3) xor tb_val(2)));
            end if;
        end if;
    end process;

    -- simulovany rychlejsi counter
    process (Clk, rst)
    begin
        if rst = '1' then
            counter_value <= 0;
        elsif (clk'event and clk = '1') then
            if count_en = '1' then
                counter_value <= counter_value + 1;
                if counter_value = C_COUNTER_DELAY then
                    counter_value <= 0;
                end if;
            end if;
        end if;
    end process;

    -- generator shift signalu
    process (count_en, counter_value)
    begin
        if ((count_en = '1') and (counter_value = C_COUNTER_DELAY)) then
            shift_tb <= '1';
        else 
            shift_tb <= '0';
        end if;
    end process;

    -- LFSR checker
    process (clk)
        variable file_line : line;
    begin
        if (clk'event and clk = '0') then
            if tb_val /= out_val and rst = '0' and shift_tb = '1' then
                write(file_line, string'("Error: Time "));
                write(file_line, now);
                write(file_line, string'(" LFSR mismatch: "));
                write(file_line, string'(" Output = 0x"));
                write(file_line, out_val);
                write(file_line, string'(" Correct = 0x"));
                write(file_line, tb_val);
                writeline(output, file_line);
                no_of_LFSR_errors_tb <= no_of_LFSR_errors_tb + 1;
            end if;
        end if;
    end process;

    stim_proc: process
        variable file_line        : line;
        variable stable           : boolean;
        variable var_no_of_errors : integer;
    begin
        terminate <= false;
        var_no_of_errors := 0;
        no_of_errors <= std_logic_vector(to_unsigned(var_no_of_errors, 8));

        -- zacatek simulace
        write(file_line, string'("### Simulation start ###"));
        writeline(output, file_line);

        rst <= '1';
        roll <= '0';
        wait until rising_edge(clk);
        wait for 1 ns;
        rst <= '0';

        -- prvni stisk - kratky - automat na nej musi zareagovat a spustit se
        wait until rising_edge(clk);
        wait for 1 ns;
        roll <= '1';
        wait until rising_edge(clk);
        wait for 1 ns;
        roll <= '0';

        wait for C_COUNTER_DELAY*CLK_PERIOD;
        wait until rising_edge(clk);
        wait for 1 ns;

        if tb_val = "0000" then
            write(file_line, string'("Error: Cas "));
            write(file_line, now);
            write(file_line, string'(" Neprobehla inicializace LFSR, vystup by nemel byt 0"));
            writeline(output, file_line);
            var_no_of_errors := var_no_of_errors + 1;
            no_of_errors <= std_logic_vector(to_unsigned(var_no_of_errors, 8));
        end if;

        wait for 2*C_COUNTER_DELAY*CLK_PERIOD;
        wait until rising_edge(clk);
        wait for 1 ns;
        stable := Count_en'stable(2*C_COUNTER_DELAY*CLK_PERIOD);
        if (stable) then
            if (count_en = '0') then
                write(file_line, string'("Error: Cas "));
                write(file_line, now);
                write(file_line, string'(" Count_en neni '1' po prvnim zmacknuti tlacitka (generovani hodnot ma bezet)"));
                writeline(output, file_line);
                var_no_of_errors := var_no_of_errors + 1;
                no_of_errors <= std_logic_vector(to_unsigned(var_no_of_errors, 8));
            end if;
        else -- not stable
            write(file_line, string'("Error: Cas "));
            write(file_line, now);
            write(file_line, string'(" Count_en neni stabilni po prvnim zmacknuti tlacitka, automat pravdepodobne reaguje na jeden stisk tlacitka vicekrat"));
            writeline(output, file_line);
            var_no_of_errors := var_no_of_errors + 1;
            no_of_errors <= std_logic_vector(to_unsigned(var_no_of_errors, 8));
        end if;

        -- druhy stisk - kratky - automat na nej musi zareagovat a zastavit se
        wait until rising_edge(clk);
        wait for 1 ns;
        roll <= '1';
        wait until rising_edge(clk);
        wait for 1 ns;
        roll <= '0';

        wait for 2*C_COUNTER_DELAY*CLK_PERIOD;
        wait until rising_edge(clk);
        wait for 1 ns;
        stable := Count_en'stable(2*C_COUNTER_DELAY*CLK_PERIOD);
        if (stable) then
            if (count_en = '1') then
                write(file_line, string'("Error: Cas "));
                write(file_line, now);
                write(file_line, string'(" Count_en neni '0' po druhem zmacknuti tlacitka (generovani hodnot ma byt zastavene)"));
                writeline(output, file_line);
                var_no_of_errors := var_no_of_errors + 1;
                no_of_errors <= std_logic_vector(to_unsigned(var_no_of_errors, 8));
            end if;
        else -- not stable
            write(file_line, string'("Error: Cas "));
            write(file_line, now);
            write(file_line, string'(" Count_en neni stabilni po druhem zmacknuti tlacitka, automat pravdepodobne reaguje na jedno uvolneni tlacitka vicekrat"));
            writeline(output, file_line);
            var_no_of_errors := var_no_of_errors + 1;
            no_of_errors <= std_logic_vector(to_unsigned(var_no_of_errors, 8));
        end if;

        -- treti stisk - dlouhy - automat na nej musi zareagovat pouze jednou a spustit se
        wait until rising_edge(clk);
        wait for 1 ns;
        roll <= '1';
        -- urcite pres jednu periodu shift, ale ne pres pres dve
        wait for C_COUNTER_DELAY*CLK_PERIOD;
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait for 1 ns;
        roll <= '0';
        
        wait until rising_edge(clk);
        wait for 1 ns;
        -- probehnout cely generator dokola
        for i in 0 to 20 loop
            stable := Count_en'stable(C_COUNTER_DELAY*CLK_PERIOD);
            if (stable) then
                if (count_en = '0') then
                    write(file_line, string'("Error: Cas "));
                    write(file_line, now);
                    write(file_line, string'(" Count_en neni '1' po tretim zmacknuti tlacitka (generovani hodnot ma bezet)"));
                    writeline(output, file_line);
                    var_no_of_errors := var_no_of_errors + 1;
                    no_of_errors <= std_logic_vector(to_unsigned(var_no_of_errors, 8));
                end if;
            else -- not stable
                write(file_line, string'("Error: Cas "));
                write(file_line, now);
                write(file_line, string'(" Count_en neni stabilni po tretim zmacknuti tlacitka, automat pravdepodobne reaguje na jeden stisk tlacitka vicekrat"));
                writeline(output, file_line);
                var_no_of_errors := var_no_of_errors + 1;
                no_of_errors <= std_logic_vector(to_unsigned(var_no_of_errors, 8));
            end if;

            if ((i > 0) and (shift_init = '1')) then -- pri prvnim kole jeste bude shift_init aktivni, ale v dalsich nesmi
                write(file_line, string'("Error: Cas "));
                write(file_line, now);
                write(file_line, string'(" Shift_init je '1' po tretim zmacknuti tlacitka (generovani hodnot ma bezet, ale inicializace uz ne)"));
                writeline(output, file_line);
                var_no_of_errors := var_no_of_errors + 1;
                no_of_errors <= std_logic_vector(to_unsigned(var_no_of_errors, 8));
            end if;

            wait for C_COUNTER_DELAY*CLK_PERIOD;
        end loop;

        -- ctvrty stisk - dlouhy - automat na nej musi zareagovat pouze jednou a zastavit se
        wait until rising_edge(clk);
        wait for 1 ns;
        roll <= '1';
        wait for C_COUNTER_DELAY*CLK_PERIOD;
        wait until rising_edge(clk);
        wait for 1 ns;
        stable := Count_en'stable(C_COUNTER_DELAY*CLK_PERIOD);
        roll <= '0';

        wait until rising_edge(clk);
        wait for 1 ns;
        if (stable) then
            if (count_en = '1') then
                write(file_line, string'("Error: Cas "));
                write(file_line, now);
                write(file_line, string'(" Count_en neni '0' po ctvrtem zmacknuti tlacitka (generovani hodnot ma byt zastavene)"));
                writeline(output, file_line);
                var_no_of_errors := var_no_of_errors + 1;
                no_of_errors <= std_logic_vector(to_unsigned(var_no_of_errors, 8));
            end if;
        else -- not stable
            write(file_line, string'("Error: Cas "));
            write(file_line, now);
            write(file_line, string'(" Count_en neni stabilni po ctvrtem zmacknuti tlacitka, automat pravdepodobne reaguje na jedno uvolneni tlacitka vicekrat"));
            writeline(output, file_line);
            var_no_of_errors := var_no_of_errors + 1;
            no_of_errors <= std_logic_vector(to_unsigned(var_no_of_errors, 8));
        end if;

        write(file_line, string'("### Total number of errors = "));
        write(file_line, var_no_of_errors + no_of_LFSR_errors_tb);
        write(file_line, string'(" (FSM errors "));
        write(file_line, var_no_of_errors);
        write(file_line, string'(") (LFSR errors "));
        write(file_line, no_of_LFSR_errors_tb);
        write(file_line, string'(")"));
        writeline(output, file_line);

        -- konec simulace
        write(file_line, string'("### Simulation finished ###"));
        writeline(output, file_line);

        terminate <= true;

        wait;

    end process;

end;
