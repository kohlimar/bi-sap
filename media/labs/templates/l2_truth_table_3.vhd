library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;

-- Ports description - inputs a, b, c, output f
-- definice vstupu a vystupu - vstupy a, b, c, vystup f
entity truth_table_3 is
    port(
        c : in  std_logic;
        b : in  std_logic;
        a : in  std_logic;
        f : out std_logic
    );
end truth_table_3;

architecture behavioral of truth_table_3 is

-- definition of the truth-table type - has 8 outputs (2^3 = 8) of standard logic type (0, 1)
-- definice typu pravdivostni tabulka - obsahuje 8 vystupu (2^3 = 8) typu standardni logika (0, 1)
    type truth_table is array (0 to 7) of std_logic;

-- definition of our function f(c, b, a) by a truth table
-- definice nasi funkce f(c, b, a) pravdivostni tabulkou
    constant our_function : truth_table := truth_table'(

-- do not change the order of the inputs a, b, c in the header - it is just a comment
-- zmena poradi vstupu a, b, c v zahlavi nema smysl - jedna se pouze o komentar

-- ------------------------------------------------------------
-- | functional value f(c, b, a)    | index |  c  |  b  |  a  |
-- | funkcni hodnota f(c, b, a)     | index |  c  |  b  |  a  |
-- ------------------------------------------------------------
                   '0',          -- |   0   |  0  |  0  |  0  |
                   '1',          -- |   1   |  0  |  0  |  1  |
                   '1',          -- |   2   |  0  |  1  |  0  |
                   '0',          -- |   3   |  0  |  1  |  1  |
                   '1',          -- |   4   |  1  |  0  |  0  |
                   '0',          -- |   5   |  1  |  0  |  1  |
                   '1',          -- |   6   |  1  |  1  |  0  |
                   '0'           -- |   7   |  1  |  1  |  1  |
-- ------------------------------------------------------------
    );

    signal address : std_logic_vector(2 downto 0);

begin
    address(2) <= c;
    address(1) <= b;
    address(0) <= a;

    f <= our_function(conv_integer(address(2 downto 0)));

end;
