library STD;
use STD.textio.all;                     -- basic I/O

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;
use IEEE.std_logic_textio.all;

entity kulicka_test is
    generic(
        C_COUNTER_DELAY : integer := 10        -- zpozdeni signalu Shift - pocet taktu
    );
    port(
        Clock   : out std_logic;
        Reset   : out std_logic;

        Run         : out std_logic;
        Stop        : in  std_logic;
        Count_en    : in  std_logic;
        en_out      : out std_logic;

        Binary      : in  std_logic_vector(3 downto 0);
        LED_left    : in  std_logic_vector(6 downto 0);
        LED_right   : in  std_logic_vector(6 downto 0)
    );  
end kulicka_test;

architecture behavior of kulicka_test is

    signal rst  : std_logic := '0';
    signal clk  : std_logic := '1';

    signal Counter_value     : integer := 0;

    signal led                  : std_logic_vector(13 downto 0);

    constant CLK_PERIOD      : time := 10 ns;
    
    signal no_of_DECODECRS_errors   : integer := 0;

    signal terminate         : boolean;
    
    type t_led is array (0 to 15) of std_logic_vector(13 downto 0);
            
    constant led_correct : t_led :=
            (B"11_1111_1111_1111",
             B"11_1111_1111_1110",
             B"11_1111_1111_1100",
             B"11_1111_1111_1000",
             B"11_1111_1111_0000",
             B"11_1111_1110_0000",
             B"11_1111_1100_0000",
             B"11_1111_1000_0000",
             B"11_1111_0000_0000",
             B"11_1111_1111_1111",
             B"11_1111_1111_1111",
             B"11_1111_1111_1111",
             B"11_1111_1111_1111",
             B"11_1111_1111_1111",
             B"11_1111_1111_1111",
             B"11_1111_1111_1111");

    ATTRIBUTE X_INTERFACE_INFO : STRING;
    ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
    ATTRIBUTE X_INTERFACE_PARAMETER OF Reset: SIGNAL IS "XIL_INTERFACENAME Reset, POLARITY ACTIVE_HIGH";
    ATTRIBUTE X_INTERFACE_INFO OF Reset: SIGNAL IS "xilinx.com:signal:reset:1.0 Reset RST";
    ATTRIBUTE X_INTERFACE_PARAMETER OF Clock: SIGNAL IS "XIL_INTERFACENAME Clock, ASSOCIATED_RESET Reset, FREQ_HZ 100000000, PHASE 0.000";
    ATTRIBUTE X_INTERFACE_INFO OF Clock: SIGNAL IS "xilinx.com:signal:clock:1.0 Clock CLK";
          
begin

    buzzer: process
        variable file_line    : line;
    begin
        wait for 1000 ns;
        if (not terminate) then
            write(file_line, string'("-->>> Simulation paused - press Run-all to continue! <<<--"));
            writeline(output, file_line);
        end if;
        wait;
    end process;
        
    process
    begin
        clk <= not clk;
        wait for CLK_PERIOD/2;
        if (terminate) then
            wait;
        end if;
    end process;

    Clock   <= clk;
    
    -- simulovany rychlejsi counter
    process (clk, rst)
    begin
        if rst = '1' then
            Counter_value <= 0;
        elsif (Clk'event and Clk = '1') then
            if Count_en = '1' then
                Counter_value <= Counter_value + 1;
                if Counter_value = C_COUNTER_DELAY then
                    Counter_value <= 0;
                end if;
            end if;
        end if;
    end process;

    en_out   <= '1' when Counter_value = C_COUNTER_DELAY else '0';

    --GFEDCBA
    --6543210
    led <= LED_left(2) & LED_left(1) & LED_left(6) & LED_right(4) & LED_right(5) & LED_right(6) & LED_left(0) & LED_right(0) & LED_right(1) & LED_right(2) & LED_right(3) & LED_left(3) & LED_left(4) & LED_left(5);

    process (Clk)
        variable file_line    : line;
    begin
        if (Clk'event and Clk = '0') then
            if led /= led_correct(conv_integer(unsigned(Binary))) and rst = '0' and Counter_value = C_COUNTER_DELAY/2 then
                write(file_line, string'("Error: Time "));
                write(file_line, now);
                write(file_line, string'(" LED outputs mismatch: "));
                write(file_line, string'(" Binary Counter = 0x"));
                write(file_line, Binary);
                write(file_line, string'(" Output = 0x"));
                write(file_line, led);
                write(file_line, string'(" Correct = 0x"));
                write(file_line, led_correct(conv_integer(unsigned(Binary))));
                writeline(output, file_line);
                no_of_DECODECRS_errors <= no_of_DECODECRS_errors + 1;
            end if;
            if Binary > B"1000" then
                write(file_line, string'("Error: Time "));
                write(file_line, now);
                write(file_line, string'(" The Binary Counter should count only to 8!"));
                writeline(output, file_line);
            end if;
        end if;
    end process;
 
    stim_proc: process
        variable file_line        : line;
        variable stable           : boolean;
        variable var_no_of_errors : integer;
    begin
        terminate <= false;
        var_no_of_errors := 0;

        -- zacatek simulace
        write(file_line, string'("### Simulation start ###"));
        writeline(output, file_line); -- write to display    


        rst <= '1';
        Run  <= '0';
        wait until rising_edge(clk);
        wait for 1 ns;
        rst <= '0';
        
        wait for 4 ns;
        if Count_en = '1' then
            write(file_line, string'("Error: Cas "));
            write(file_line, now);
            write(file_line, string'(" Po resetu by mel byt Enable roven 0"));
            writeline(output, file_line);
            var_no_of_errors := var_no_of_errors + 1;
        end if;
        if Stop = '1' then
            write(file_line, string'("Error: Cas "));
            write(file_line, now);
            write(file_line, string'(" Po resetu by nemel byt nastaven treshold jadra Binary Counter"));
            writeline(output, file_line);
            var_no_of_errors := var_no_of_errors + 1;
        end if;
        -- prvni stisk - kratky - automat na nej musi zareagovat spustit se a nechat dojet citac
        wait until rising_edge(clk);
        wait for 1 ns;
        Run <= '1';
        wait until rising_edge(clk);
        wait for 1 ns;
        Run <= '0';


         wait for 8*C_COUNTER_DELAY*CLK_PERIOD;
         wait until rising_edge(clk);
         wait for 1 ns;


        stable := Count_en'stable(8*C_COUNTER_DELAY*CLK_PERIOD);
        if (stable) then
            if (Count_en = '0') then
                write(file_line, string'("Error: Cas "));
                write(file_line, now);
                write(file_line, string'(" Count_en neni '1' po prvnim zmacknuti tlacitka (citac ma bezet)"));
                writeline(output, file_line);
                var_no_of_errors := var_no_of_errors + 1;
            end if;
        else -- not stable
            write(file_line, string'("Error: Cas "));
            write(file_line, now);
            write(file_line, string'(" Count_en neni stabilni po prvnim zmacknuti tlacitka, automat pravdepodobne reaguje na jeden stisk tlacitka vicekrat"));
            writeline(output, file_line);
            var_no_of_errors := var_no_of_errors + 1;
        end if;

        wait for 2*C_COUNTER_DELAY*CLK_PERIOD;
        wait until rising_edge(clk);
        wait for 1 ns;

        if Stop = '0' then
            write(file_line, string'("Error: Cas "));
            write(file_line, now);
            write(file_line, string'(" Treshold Binary Counteru by mel byt v 1! Spatne nastaveny treshold, nebo nestabilni Count_en."));
            writeline(output, file_line);
            var_no_of_errors := var_no_of_errors + 1;
        end if;

        -- -- druhy stisk - dlouhy - automat na nej musi zareagovat a dvakrat protocit kulicku
        wait until rising_edge(clk);
        wait for 1 ns;
        Run <= '1';
        wait for 13*C_COUNTER_DELAY*CLK_PERIOD;
        wait until rising_edge(clk);
        wait for 1 ns;
        Run <= '0';

        wait for 5*C_COUNTER_DELAY*CLK_PERIOD;
        wait until rising_edge(clk);
        wait for 1 ns;
        stable := Count_en'stable(14*C_COUNTER_DELAY*CLK_PERIOD);
        if (stable) then
            if (Count_en = '0') then
                write(file_line, string'("Error: Cas "));
                write(file_line, now);
                write(file_line, string'(" Count_en neni '1' po druhem zmacknuti tlacitka (citac ma bezet)"));
                writeline(output, file_line);
                var_no_of_errors := var_no_of_errors + 1;
            end if;
        else -- not stable
            write(file_line, string'("Error: Cas "));
            write(file_line, now);
            write(file_line, string'(" Count_en neni stabilni po prvnim zmacknuti tlacitka, automat pravdepodobne reaguje na jeden stisk tlacitka vicekrat"));
            writeline(output, file_line);
            var_no_of_errors := var_no_of_errors + 1;
        end if;

        wait for 2*C_COUNTER_DELAY*CLK_PERIOD;
        wait until rising_edge(clk);
        wait for 1 ns;

        if Stop = '0' then
            write(file_line, string'("Error: Cas "));
            write(file_line, now);
            write(file_line, string'(" Treshold Binary Counteru by mel byt v 1! Spatne nastaveny treshold, nebo nestabilni Count_en."));
            writeline(output, file_line);
            var_no_of_errors := var_no_of_errors + 1;
        end if;

        write(file_line, string'("### Total number of errors = "));
        write(file_line, var_no_of_errors + no_of_DECODECRS_errors);
        write(file_line, string'(" (FSM errors "));
        write(file_line, var_no_of_errors);
        write(file_line, string'(") (7seg-decoders errors "));
        write(file_line, no_of_DECODECRS_errors);
        write(file_line, string'(")"));
        writeline(output, file_line);

        -- konec simulace
        write(file_line, string'("### Simulation finished ###"));
        writeline(output, file_line);

        terminate <= true;

        wait;
    end process;

    Reset   <= rst;


end;
