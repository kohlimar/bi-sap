library IEEE;
use IEEE.std_logic_1164.all;

entity decoder_7seg_right is
--entity decoder_7seg_left is
    port(
        binary : in  std_logic_vector (3 downto 0);
        LED    : out std_logic_vector (6 downto 0)
    );
end decoder_7seg_right;
--end decoder_7seg_left;

architecture behavioral of decoder_7seg_right is
--architecture behavioral of decoder_7seg_left is

begin

    -- poradi segmentu
    --      GFEDCBA
    with binary select
    LED <= "1111111" when "0000", -- 0
           "1111111" when "0001", -- 1
           "1111111" when "0010", -- 2
           "1111111" when "0011", -- 3
           "1111111" when "0100", -- 4
           "1111111" when "0101", -- 5
           "1111111" when "0110", -- 6
           "1111111" when "0111", -- 7
           "1111111" when "1000", -- 8
           "1111111" when "1001", -- 9
           "1111111" when "1010", -- A
           "1111111" when "1011", -- b
           "1111111" when "1100", -- C
           "1111111" when "1101", -- d
           "1111111" when "1110", -- E
           "1111111" when "1111", -- F
           "1111111" when others; -- else

end;
