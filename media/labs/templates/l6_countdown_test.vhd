library STD;
use STD.textio.all;                     -- basic I/O

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;
use IEEE.std_logic_textio.all;

entity countdown_test is
    generic(
        C_COUNTER_DELAY : integer := 10        -- zpozdeni signalu Shift - pocet taktu
    );
    port(
        Clock   : out std_logic;
        Reset   : out std_logic;

        Run         : out std_logic;
        Stop        : in  std_logic;
        en_out      : out std_logic;
        Count_en    : in  std_logic;
        Load        : in  std_logic;
        Finish      : in  std_logic;

        Value       : out std_logic_vector(7 downto 0);
        Binary      : in  std_logic_vector(7 downto 0)
    );  
end countdown_test;

architecture behavior of countdown_test is

    signal rst  : std_logic := '0';
    signal clk  : std_logic := '1';

    signal Counter_value     : integer := 0;

    signal bin_val           : std_logic_vector(7 downto 0) := X"00";

    constant CLK_PERIOD      : time := 10 ns;
    
    signal terminate         : boolean;
                
    ATTRIBUTE X_INTERFACE_INFO : STRING;
    ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
    ATTRIBUTE X_INTERFACE_PARAMETER OF Reset: SIGNAL IS "XIL_INTERFACENAME Reset, POLARITY ACTIVE_HIGH";
    ATTRIBUTE X_INTERFACE_INFO OF Reset: SIGNAL IS "xilinx.com:signal:reset:1.0 Reset RST";
    ATTRIBUTE X_INTERFACE_PARAMETER OF Clock: SIGNAL IS "XIL_INTERFACENAME Clock, ASSOCIATED_RESET Reset, FREQ_HZ 100000000, PHASE 0.000";
    ATTRIBUTE X_INTERFACE_INFO OF Clock: SIGNAL IS "xilinx.com:signal:clock:1.0 Clock CLK";
          
begin

    buzzer: process
        variable file_line    : line;
    begin
        wait for 1000 ns;
        if (not terminate) then
            write(file_line, string'("-->>> Simulation paused - press Run-all to continue! <<<--"));
            writeline(output, file_line);
        end if;
        wait;
    end process;
        
    process
    begin
        clk <= not clk;
        wait for CLK_PERIOD/2;
        if (terminate) then
            wait;
        end if;
    end process;

    Clock   <= clk;
    
    -- simulovany rychlejsi counter
    process (clk, rst)
    begin
        if rst = '1' then
            Counter_value <= 0;
        elsif (Clk'event and Clk = '1') then
            if Count_en = '1' then
                Counter_value <= Counter_value + 1;
                if Counter_value = C_COUNTER_DELAY then
                    Counter_value <= 0;
                end if;
            end if;
        end if;
    end process;

    en_out   <= '1' when Counter_value = C_COUNTER_DELAY else '0';

    process (Clk)
        variable file_line    : line;
    begin
        if (Clk'event and Clk = '0') then
            if bin_val /= Binary and rst = '0' then
                write(file_line, string'("Binary Counter changed = 0x"));
                hwrite(file_line, Binary);
                writeline(output, file_line);
            end if;
            bin_val <= Binary;
        end if;
    end process;
 
    stim_proc: process
        variable file_line        : line;
        variable stable           : boolean;
        variable var_no_of_errors : integer;
    begin
        terminate <= false;
        var_no_of_errors := 0;

        -- zacatek simulace
        write(file_line, string'("### Simulation start ###"));
        writeline(output, file_line); -- write to display    


        rst <= '1';
        Run  <= '0';
        Value <= X"00";
        wait until rising_edge(clk);
        wait for 1 ns;
        rst <= '0';
        
        wait for 4 ns;
        if Count_en = '1' then
            write(file_line, string'("Error: Cas "));
            write(file_line, now);
            write(file_line, string'(" Po resetu by mel byt Enable roven 0"));
            writeline(output, file_line);
            var_no_of_errors := var_no_of_errors + 1;
        end if;
        if Finish = '1' then
            write(file_line, string'("Error: Cas "));
            write(file_line, now);
            write(file_line, string'(" Po resetu by nemel byt nastaven signal Finish"));
            writeline(output, file_line);
            var_no_of_errors := var_no_of_errors + 1;
        end if;

        -- prvni stisk - kratky - automat na nej musi zareagovat spustit se a nechat dojet citac
        wait until rising_edge(clk);
        Value   <= X"04";
        wait until rising_edge(clk);
        wait for 1 ns;
        Run <= '1';
        wait until rising_edge(clk);
        wait for 1 ns;
        Run <= '0';


         wait for 4*C_COUNTER_DELAY*CLK_PERIOD;
         wait until rising_edge(clk);
         wait for 1 ns;


        stable := Count_en'stable(4*C_COUNTER_DELAY*CLK_PERIOD);
        if (stable) then
            if (Count_en = '0') then
                write(file_line, string'("Error: Cas "));
                write(file_line, now);
                write(file_line, string'(" Count_en neni '1' po prvnim zmacknuti tlacitka (citac ma bezet)"));
                writeline(output, file_line);
                var_no_of_errors := var_no_of_errors + 1;
            end if;
        else -- not stable
            write(file_line, string'("Error: Cas "));
            write(file_line, now);
            write(file_line, string'(" Count_en neni stabilni po prvnim zmacknuti tlacitka, automat pravdepodobne reaguje na jeden stisk tlacitka vicekrat"));
            writeline(output, file_line);
            var_no_of_errors := var_no_of_errors + 1;
        end if;

        wait for 1*C_COUNTER_DELAY*CLK_PERIOD;
        wait until rising_edge(clk);
        wait for 1 ns;

        if Stop = '0' then
            write(file_line, string'("Error: Cas "));
            write(file_line, now);
            write(file_line, string'(" Treshold Binary Counteru by mel byt v 1! Spatne nastaveny treshold, nebo nestabilni Count_en."));
            writeline(output, file_line);
            var_no_of_errors := var_no_of_errors + 1;
        end if;

        if Finish = '0' and Stop = '1' then
            write(file_line, string'("Error: Cas "));
            write(file_line, now);
            write(file_line, string'(" Finish by mel byt v 1! Countdown uz ma nastaveny treshold (stop signal)."));
            writeline(output, file_line);
            var_no_of_errors := var_no_of_errors + 1;
        end if;

         -- druhy stisk - kratky - automat na nej musi zareagovat a vratit se do stavu nahravani hodnoty
         wait until rising_edge(clk);
         wait for 1 ns;
         Run <= '1';
         wait until rising_edge(clk);
         wait for 1 ns;
         Run <= '0';
 
         wait for C_COUNTER_DELAY*CLK_PERIOD;
         wait until rising_edge(clk);
         wait for 1 ns;
         stable := Count_en'stable(C_COUNTER_DELAY*CLK_PERIOD);
         if (stable) then
             if (Count_en = '1') then
                 write(file_line, string'("Error: Cas "));
                 write(file_line, now);
                 write(file_line, string'(" Count_en neni '0' po druhem zmacknuti tlacitka"));
                 writeline(output, file_line);
                 var_no_of_errors := var_no_of_errors + 1;
             end if;
         else -- not stable
             write(file_line, string'("Error: Cas "));
             write(file_line, now);
             write(file_line, string'(" Count_en neni stabilni po druhem zmacknuti tlacitka, automat pravdepodobne reaguje na jedno uvolneni tlacitka vicekrat"));
             writeline(output, file_line);
             var_no_of_errors := var_no_of_errors + 1;
         end if;

         if Finish = '1' then
            write(file_line, string'("Error: Cas "));
            write(file_line, now);
            write(file_line, string'(" Finish by mel byt v 0! Automat by mel byt v pocatecnim stavu."));
            writeline(output, file_line);
            var_no_of_errors := var_no_of_errors + 1;
        end if;

        if Load = '0' then
            write(file_line, string'("Error: Cas "));
            write(file_line, now);
            write(file_line, string'(" Load by mel byt v 1! Automat je v pocatecnim stavu a nastavovana hodnota by se mela zobrazovat."));
            writeline(output, file_line);
            var_no_of_errors := var_no_of_errors + 1;
        end if;

        -- test - load
        wait until rising_edge(clk); wait for 1 ns;
        Value   <= X"BE";
        wait until rising_edge(clk); wait for 1 ns;
        if Binary /= X"BE" then
            write(file_line, string'("Error: Cas "));
            write(file_line, now);
            write(file_line, string'(" Load by mel byt v 1! Automat je v pocatecnim stavu a nastavovana hodnota by se mela zobrazovat."));
            writeline(output, file_line);
            var_no_of_errors := var_no_of_errors + 1;
        end if;
        Value   <= X"EF";
        wait until rising_edge(clk); wait for 1 ns;
        if Binary /= X"EF" then
            write(file_line, string'("Error: Cas "));
            write(file_line, now);
            write(file_line, string'(" Load by mel byt v 1! Automat je v pocatecnim stavu a nastavovana hodnota by se mela zobrazovat."));
            writeline(output, file_line);
            var_no_of_errors := var_no_of_errors + 1;
        end if;

        -- treti stisk - kratky - automat na nej musi zareagovat spustit se a nechat dojet citac
        Value   <= X"10";
        wait until rising_edge(clk);
        
        wait until rising_edge(clk);
        wait for 1 ns;
        Run <= '1';
        wait until rising_edge(clk);
        wait for 1 ns;
        Run <= '0';


         wait for 17*C_COUNTER_DELAY*CLK_PERIOD;
         wait until rising_edge(clk);
         wait for 1 ns;


        stable := Count_en'stable(17*C_COUNTER_DELAY*CLK_PERIOD);
        if (stable) then
            if (Count_en = '0') then
                write(file_line, string'("Error: Cas "));
                write(file_line, now);
                write(file_line, string'(" Count_en neni '1' po tretim zmacknuti tlacitka (citac ma znovu bezet)"));
                writeline(output, file_line);
                var_no_of_errors := var_no_of_errors + 1;
            end if;
        else -- not stable
            write(file_line, string'("Error: Cas "));
            write(file_line, now);
            write(file_line, string'(" Count_en neni stabilni po tretim zmacknuti tlacitka, automat pravdepodobne reaguje na jeden stisk tlacitka vicekrat"));
            writeline(output, file_line);
            var_no_of_errors := var_no_of_errors + 1;
        end if;

        wait for 1*C_COUNTER_DELAY*CLK_PERIOD;
        wait until rising_edge(clk);
        wait for 1 ns;

        if Stop = '0' then
            write(file_line, string'("Error: Cas "));
            write(file_line, now);
            write(file_line, string'(" Treshold Binary Counteru by mel byt v 1! Spatne nastaveny treshold, nebo nestabilni Count_en."));
            writeline(output, file_line);
            var_no_of_errors := var_no_of_errors + 1;
        end if;

        if Finish = '0' and Stop = '1' then
            write(file_line, string'("Error: Cas "));
            write(file_line, now);
            write(file_line, string'(" Finish by mel byt v 1! Countdown uz ma nastaveny treshold (stop signal)."));
            writeline(output, file_line);
            var_no_of_errors := var_no_of_errors + 1;
        end if;

         -- ctvrty stisk - kratky - automat na nej musi zareagovat a vratit se do stavu nahravani hodnoty
         wait until rising_edge(clk);
         wait for 1 ns;
         Run <= '1';
         wait until rising_edge(clk);
         wait for 1 ns;
         Run <= '0';
 
         wait for C_COUNTER_DELAY*CLK_PERIOD;
         wait until rising_edge(clk);
         wait for 1 ns;
         stable := Count_en'stable(C_COUNTER_DELAY*CLK_PERIOD);
         if (stable) then
             if (Count_en = '1') then
                 write(file_line, string'("Error: Cas "));
                 write(file_line, now);
                 write(file_line, string'(" Count_en neni '0' po ctvrtem zmacknuti tlacitka"));
                 writeline(output, file_line);
                 var_no_of_errors := var_no_of_errors + 1;
             end if;
         else -- not stable
             write(file_line, string'("Error: Cas "));
             write(file_line, now);
             write(file_line, string'(" Count_en neni stabilni po druhem zmacknuti tlacitka, automat pravdepodobne reaguje na jedno uvolneni tlacitka vicekrat"));
             writeline(output, file_line);
             var_no_of_errors := var_no_of_errors + 1;
         end if;

         if Finish = '1' then
            write(file_line, string'("Error: Cas "));
            write(file_line, now);
            write(file_line, string'(" Finish by mel byt v 0! Automat by mel byt v pocatecnim stavu."));
            writeline(output, file_line);
            var_no_of_errors := var_no_of_errors + 1;
        end if;

        if Load = '0' then
            write(file_line, string'("Error: Cas "));
            write(file_line, now);
            write(file_line, string'(" Load by mel byt v 1! Automat je v pocatecnim stavu a nastavovana hodnota by se mela zobrazovat."));
            writeline(output, file_line);
            var_no_of_errors := var_no_of_errors + 1;
        end if;

        write(file_line, string'("### Total number of errors = "));
        write(file_line, var_no_of_errors);
        write(file_line, string'(" (FSM errors) ###"));
        writeline(output, file_line);
        -- konec simulace
        write(file_line, string'("### Simulation finished ###"));
        writeline(output, file_line);

        terminate <= true;

        wait;
    end process;

    Reset   <= rst;


end;
